#!/bin/sh
cd medblocksbackend
go mod vendor
cd ..
sudo docker build -f Dockerfile -t medblocks_dev_base .
cd medblocksbackend
sudo docker-compose build -t medblocks/go-medblocks
sudo docker-compose up &
sleep 30

#initializing couchDB as single node
curl -X PUT http://localhost:5984/_users
curl -X PUT http://localhost:5984/_replicator

#adding databases to couchDB
curl -X PUT http://localhost:5984/users

sudo docker-compose down
