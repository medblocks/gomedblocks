FROM alpine AS multistage
ENV GOPATH=/go
ENV GOBIN=$GOPATH/bin
RUN apk update
RUN apk add go git musl-dev libbsd-dev bash
ADD ./medblocksbackend/ /go/src/gitlab.com/medblocks/gomedblocks/medblocksbackend
WORKDIR /go/src/gitlab.com/medblocks/gomedblocks/medblocksbackend
RUN go install ./cmd/MedblocksBackend.go

FROM alpine
COPY --from=multistage /go/bin/MedblocksBackend /go/bin/MedblocksBackend
EXPOSE 8080
CMD ["/go/bin/MedblocksBackend", "-test", "-docker"]
