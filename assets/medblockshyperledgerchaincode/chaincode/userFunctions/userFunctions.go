/*
userFunctions.go contains RegisterUserSmartContract and implements the following functions
*queryUser (Args:EmailId)
*registerUser (Args: SPublicKey,EPublicKey,EmailId,Metadata-optional)
*queryAllUsers (Args:nil)
*changeUserDetails (Args: SPublicKey,EPublicKey,EmailId,Metadata-optional) cannot change emailId as of now
*/
package userFunctions

import (
	"bytes"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	b64 "encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"

	"crypto"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type RegisterUserSmartContract struct {
}

// Define the User structure, with 4 properties.  Structure tags are used by encoding/json library
type User struct {
	SPublicKey       string `json:"sPublicKey"`
	EPublicKey       string `json:"ePublicKey"`
	EmailId          string `json:"emailId"`
	IdentityFileHash string `json:"identityFileHash,omitempty"`
	Metadata         string `json:"metadata"`
}

/*
 * The Init method is called when the Smart Contract "userFunctions" is instantiated by the blockchain network
 */
func (R *RegisterUserSmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "userFunctions.go"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (R *RegisterUserSmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryUser" {
		return R.queryUser(APIstub, args)
	} else if function == "registerUser" {
		return R.registerUser(APIstub, args)
	} else if function == "queryAllUsers" {
		return R.queryAllUsers(APIstub)
	} else if function == "updateIdentityFile" {
		return R.updateIdentityFile(APIstub, args)
	}

	return shim.Error(fmt.Sprint("Invalid Smart Contract function name:", function))
}

func (R *RegisterUserSmartContract) queryUser(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	userAsBytes, err := APIstub.GetState(args[0])
	if err != nil {
		return shim.Error("Error in GetState!")
	}
	if userAsBytes == nil {
		return shim.Error("User doesn't exist!")
	}
	return shim.Success(userAsBytes)
}

func (R *RegisterUserSmartContract) registerUser(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if (len(args) != 4) && (len(args) != 3) {
		return shim.Error(fmt.Sprint("Incorrect number of arguments. Expecting 3 or 4, sent: ", args))
	}
	var user User
	if len(args) == 3 {
		user = User{EmailId: args[0], EPublicKey: args[1], SPublicKey: args[2], Metadata: ""}
	} else {
		user = User{EmailId: args[0], EPublicKey: args[1], SPublicKey: args[2], Metadata: args[3]}
	}
	userAsBytes, err := APIstub.GetState(args[0])
	if err != nil {
		return shim.Error("Error in GetState!")
	}
	if userAsBytes != nil {
		return shim.Error("User already exists!")
	}
	// eBlock, _ := pem.Decode([]byte(user.EPublicKey))
	// sBlock, _ := pem.Decode([]byte(user.SPublicKey))
	// if eBlock == nil {
	// 	return shim.Error("Encryption public key not PEM")
	// }
	// if sBlock == nil {
	// 	return shim.Error("Signature public key not PEM")
	// }
	// _, eErr := x509.ParsePKIXPublicKey(eBlock.Bytes)
	// _, sErr := x509.ParsePKIXPublicKey(sBlock.Bytes)
	// if eErr != nil {
	// 	return shim.Error("Encryption public key not pkcs")
	// }
	// if sErr != nil {
	// 	return shim.Error("Signature public key not pkcs")
	// }
	// fmt.Printf("-registerUser:\n%s,%s,%s,%s\n", user.SPublicKey, user.EPublicKey, user.EmailId, user.Metadata)
	userAsBytes, err = json.Marshal(user)
	if err != nil {
		shim.Error("Unable to Marshal json!")
	}
	fmt.Printf("userAsBytes:%s\n", string(userAsBytes))
	err = APIstub.PutState(args[0], userAsBytes)
	if err != nil {
		shim.Error("Error in PutState!")
	}
	return shim.Success(userAsBytes)
}

func (R *RegisterUserSmartContract) queryAllUsers(APIstub shim.ChaincodeStubInterface) sc.Response {
	startKey := ""
	endKey := ""
	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllUsers:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (R *RegisterUserSmartContract) updateIdentityFile(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}
	var req, user User
	data := args[0]
	sig := args[1]
	json.Unmarshal([]byte(data), &req)
	err := verifySignature(APIstub, req.EmailId, sig, data)
	if err != nil {
		shim.Error(err.Error())
	}
	userAsBytes, err := APIstub.GetState(req.EmailId)
	if err != nil {
		shim.Error(err.Error())
	}
	err = json.Unmarshal(userAsBytes, &user)
	if err != nil {
		shim.Error("Error in unmarshal!")
	}
	user.IdentityFileHash = req.IdentityFileHash
	userAsBytes, _ = json.Marshal(user)
	APIstub.PutState(req.EmailId, userAsBytes)
	return shim.Success(userAsBytes)
}

/* verifySignature: verify if the message is signed by user with ownerEmailID */
func verifySignature(APIstub shim.ChaincodeStubInterface, ownerEmailID string, signature string, message string) (err error) {
	fmt.Println("here")

	userAsBytes, err := APIstub.GetState(ownerEmailID)
	if err != nil {
		return
	}
	var user User
	err = json.Unmarshal([]byte(userAsBytes), &user)
	if err != nil {
		return fmt.Errorf("Unmarshalling error: %s", err)
	}
	fmt.Println(user.SPublicKey)
	block, _ := pem.Decode([]byte(user.SPublicKey))
	if block == nil {
		return fmt.Errorf("failed to decode PEM block containing public key")
	}
	fmt.Println("here1")
	pubKey, parseErr := x509.ParsePKIXPublicKey(block.Bytes)
	fmt.Println("here2")
	if parseErr != nil {
		return fmt.Errorf("Failed to parse public key: %s", parseErr)
	}
	fmt.Println("here3")
	rsaPubKey := pubKey.(*rsa.PublicKey)

	fmt.Println("here6")
	h := sha256.New()
	h.Write([]byte(message))
	digest := h.Sum(nil)
	decodedSignatureBytes, _ := b64.StdEncoding.DecodeString(signature)
	verifyErr := rsa.VerifyPKCS1v15(rsaPubKey, crypto.SHA256, digest, decodedSignatureBytes)
	return verifyErr
}
