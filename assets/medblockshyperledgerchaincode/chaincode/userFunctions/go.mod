module gitlab.com/medblocks/medblockshyperledgerchaincode/userFunctions

go 1.12

require (
	github.com/golang/protobuf v0.0.0-20181030154721-1918e1ff6ffd
	github.com/hyperledger/fabric v0.0.0-20181030160221-60f968db8e6e
	github.com/pkg/errors v0.0.0-20181023235946-059132a15dd0
)
