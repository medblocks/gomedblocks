package FileAssetHandlers

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"

	//	"math/big"
	b64 "encoding/base64"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

// FileAssetHandlers implements the chaincode to handle files
type FileAssetHandlers struct {
}

//Structures to represent assets and JSON objects received

type Permission struct {
	EmailID string `json:"receiverEmailId"`
	AesKey  string `json:"receiverKey"`
}
type Signatory struct {
	EmailID string `json:emailId`
}
type FileAsset struct {
	IpfsHash         string       `json:"ipfsHash"`
	OwnerEmailID     string       `json:"ownerEmailId"`
	Signatories      []Signatory  `json:"signatories, omitempty"`
	SignatoryEmailID string       `json:"signatoryEmailId, omitempty"`
	Name             string       `json:"name"`
	IV               string       `json:"IV"`
	Format           string       `json:"format"`
	Permissions      []Permission `json:"permissions"`
}

type SignedAuthorizationRequest struct {
	RequestMessage string
	Signature      string
}

type AuthorizationRequest struct {
	IpfsHash      string       `json:"ipfsHash"`
	SenderEmailID string       `json:"senderEmailId"`
	Permissions   []Permission `json:"permissions"`
}

type UserAsset struct {
	SPublicKey       string `json:"sPublicKey"`
	EPublicKey       string `json:"ePublicKey"`
	EmailID          string `json:"emailId"`
	IdentityFileHash string `json:"identityFileHash,omitempty`
	Metadata         string `json:"metadata,omitempty"`
}

type FileListRequest struct {
	OwnerEmailID     string `json:"ownerEmailId"`
	PermittedEmailID string `json:"receiverEmailId"`
}

type FileListRecord struct {
	IpfsHash     string       `json:"ipfsHash"`
	Name         string       `json:"name"`
	Format       string       `json:"format"`
	OwnerEmailID string       `json:"ownerEmailId"`
	Permissions  []Permission `json:"permissions"`
}

type FileList struct {
	List []FileListRecord `json:"list"`
}

func (t *FileAssetHandlers) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func (t *FileAssetHandlers) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	fn, args := stub.GetFunctionAndParameters()
	var result string
	var err error
	if fn == "addFile" {
		result, err = addFile(stub, args)
	} else if fn == "getFile" {
		result, err = getFile(stub, args)
	} else if fn == "authorizeUser" {
		result, err = authorizeUser(stub, args)
	} else if fn == "getFileList" {
		result, err = getFileList(stub, args)
	} else {
		return shim.Error("Unknown function")
	}

	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println(result)

	return shim.Success([]byte(result))
}
func toChaincodeArgs(args ...string) [][]byte {
	bargs := make([][]byte, len(args))
	for i, arg := range args {
		bargs[i] = []byte(arg)
	}
	return bargs
}
func checkUserExists(stub shim.ChaincodeStubInterface, email string) bool {
	chaincodeArgs := toChaincodeArgs("queryUser", email)
	queryUserResponse := stub.InvokeChaincode("userF", chaincodeArgs, stub.GetChannelID())
	if queryUserResponse.GetPayload() != nil {
		return true
	}
	return false
}
func checkFileExists(stub shim.ChaincodeStubInterface, hash string) bool {
	filestr, err := stub.GetState(hash)
	if err != nil || filestr == nil {
		return false
	}
	return true
}

/* addFile: create new file asset on the blockchain without any authorized users
 * Inputs passed through args: All attributed declared in FileAsset except Permissions
 */
func addFile(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Response struct {
		IpfsHash string `json:"ipfsHash"`
		Message  string `json:"message"`
		Err      error  `json:"error"`
	}
	var Responses []Response
	if len(args) != 2 {
		return "", fmt.Errorf("Incorrect number of arguments. Expected 2 arguments got %d", len(args))
	}
	var files []FileAsset
	unmarshalErr := json.Unmarshal([]byte(args[0]), &files)
	fmt.Println("Signature: ", args[1])
	fmt.Println("Message: ", args[0])

	if !checkUserExists(stub, files[0].SignatoryEmailID) {
		return "", fmt.Errorf("invalid Users used")
	}
	verifyErr := verifySignature(stub, files[0].SignatoryEmailID, args[1], args[0])
	if verifyErr != nil {
		return "", fmt.Errorf("Signature verification failed: %s", verifyErr)
	}
	if unmarshalErr != nil {
		return "", fmt.Errorf("Unmarshalling error: %s", unmarshalErr)
	}
	for _, file := range files {
		var updatedFileJson []byte
		if checkFileExists(stub, file.IpfsHash) {
			storedFileJson, err := getFile(stub, []string{file.IpfsHash})
			if err != nil {
				Responses = append(Responses, Response{IpfsHash: file.IpfsHash, Message: "", Err: fmt.Errorf("Error in GetFile: %s", err)})

				continue
			}
			var storedFile FileAsset
			json.Unmarshal([]byte(storedFileJson), &storedFile)
			storedFile.Signatories = append(storedFile.Signatories, Signatory{EmailID: file.SignatoryEmailID})
			updatedFileJson, _ = json.Marshal(storedFile)
		} else {
			if file.IpfsHash == "" || file.Name == "" || file.Format == "" || file.SignatoryEmailID == "" || file.OwnerEmailID == "" || file.Permissions == nil || file.IV == "" {
				missing := "Missing Required fields:"
				if file.IpfsHash == "" {
					missing += " ipfsHash"
				}
				if file.Name == "" {
					missing += " name"
				}
				if file.Format == "" {
					missing += " format"
				}
				if file.SignatoryEmailID == "" {
					missing += " signatoryEmailId"
				}
				if file.OwnerEmailID == "" {
					missing += " ownerEmailId"
				}
				if file.Permissions == nil {
					missing += " permissions"
				}
				if file.IV == "" {
					missing += " IV"
				}
				Responses = append(Responses, Response{IpfsHash: file.IpfsHash, Message: "", Err: fmt.Errorf("%s", missing)})
				continue
			}
			if !checkUserExists(stub, file.OwnerEmailID) {
				Responses = append(Responses, Response{IpfsHash: file.IpfsHash, Message: "", Err: fmt.Errorf("invalid Users used")})

				continue
			}
			file.Signatories = []Signatory{Signatory{EmailID: file.SignatoryEmailID}}
			file.SignatoryEmailID = ""
			updatedFileJson, _ = json.Marshal(file)
		}
		err2 := stub.PutState(file.IpfsHash, updatedFileJson)
		if err2 != nil {
			Responses = append(Responses, Response{IpfsHash: file.IpfsHash, Message: "", Err: fmt.Errorf("Failed to add asset: %s", err2)})

			continue
		}
		Responses = append(Responses, Response{IpfsHash: file.IpfsHash, Message: string(updatedFileJson), Err: nil})
	}
	ret, _ := json.Marshal(Responses)
	return string(ret), nil
}

/* getFile: get file asset with given key
 * Inputs passed through args:
 *      - The key used to store the file asset. [of the form ownerID.IpfsHash ]
 */
func getFile(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect number of parameters. Expected 1 argument, got %d", len(args))
	}

	filestr, err := stub.GetState(args[0])

	if err != nil {
		return "", fmt.Errorf("Unable to get file asset %s with error: %s", args[0], err)
	}
	if filestr == nil {
		return "", fmt.Errorf("File asset not found: %s", args[0])
	}
	fmt.Println(filestr)
	return string(filestr), nil
}

/* verifySignature: verify if the message is signed by user with ownerEmailID */
func verifySignature(stub shim.ChaincodeStubInterface, ownerEmailID string, signature string, message string) error {
	fmt.Println("here")
	chaincodeArgs := toChaincodeArgs("queryUser", ownerEmailID)
	queryUserResponse := stub.InvokeChaincode("userF", chaincodeArgs, stub.GetChannelID())
	if queryUserResponse.Status != shim.OK {
		return fmt.Errorf("Error requesting file owner details: %s", queryUserResponse.Message)
	}
	userAsBytes := queryUserResponse.GetPayload()
	var user UserAsset
	err := json.Unmarshal([]byte(userAsBytes), &user)
	if err != nil {
		return fmt.Errorf("Unmarshalling error: %s", err)
	}
	fmt.Println(user.SPublicKey)
	block, _ := pem.Decode([]byte(user.SPublicKey))
	if block == nil {
		return fmt.Errorf("failed to decode PEM block containing public key")
	}
	fmt.Println("here1")
	pubKey, parseErr := x509.ParsePKIXPublicKey(block.Bytes)
	fmt.Println("here2")
	if parseErr != nil {
		return fmt.Errorf("Failed to parse public key: %s", parseErr)
	}
	fmt.Println("here3")
	rsaPubKey := pubKey.(*rsa.PublicKey)

	fmt.Println("here6")
	h := sha256.New()
	h.Write([]byte(message))
	digest := h.Sum(nil)
	decodedSignatureBytes, _ := b64.StdEncoding.DecodeString(signature)
	verifyErr := rsa.VerifyPKCS1v15(rsaPubKey, crypto.SHA256, digest, decodedSignatureBytes)
	return verifyErr
}

/* authorizeUser: add an authorized user to a file who's authorized to read the file stored on IPFS
 * Inputs passed through args:
 *      - an authorization request message with the same format as AuthorizationRequest type
 *      - RSA PKCS#1 v1.5 signature string
 */
func authorizeUser(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 2 {
		return "", fmt.Errorf("Incorrect number of arguments. Expected 2 arguments, got %d", len(args))
	}

	var request AuthorizationRequest
	unmarshalErr := json.Unmarshal([]byte(args[0]), &request)
	if unmarshalErr != nil {
		return "", fmt.Errorf("Unmarshalling error: %s", unmarshalErr)
	}

	fileString, err := getFile(stub, []string{request.IpfsHash})
	if err != nil {
		return "", err
	}
	if fileString == "" {
		return "", fmt.Errorf("File not found")
	}
	var file FileAsset

	unmarshalErr = json.Unmarshal([]byte(fileString), &file)
	if unmarshalErr != nil {
		return "", fmt.Errorf("Unmarshalling error: %s", unmarshalErr)
	}
	fmt.Println(request)
	fmt.Println(file)
	verifyErr := verifySignature(stub, file.OwnerEmailID, args[1], args[0])
	if verifyErr != nil {
		return "", fmt.Errorf("Signature verification failed: %s", verifyErr)
	}
	var addedPerms []Permission
	for _, vr := range request.Permissions {
		repeat := false
		for _, vf := range file.Permissions {
			fmt.Println("R: ", vr.EmailID, "  F: ", vf.EmailID)
			if vf.EmailID == vr.EmailID {
				repeat = true
				break
			}
		}
		if !repeat {
			if checkUserExists(stub, vr.EmailID) {
				file.Permissions = append(file.Permissions, vr)
				addedPerms = append(addedPerms, vr)
			}
		}
	}
	fileAsBytes, err := json.Marshal(&file)
	fmt.Println(string(fileAsBytes))
	putStateErr := stub.PutState(file.IpfsHash, fileAsBytes)
	if putStateErr != nil {
		return "", fmt.Errorf("Error adding authorized user: %s", putStateErr)
	}
	resp, _ := json.Marshal(addedPerms)
	return string(resp), nil
}

func getFileListFromResultSet(resultSetIterator shim.StateQueryIteratorInterface) (string, error) {
	var list FileList
	for {
		if !resultSetIterator.HasNext() {
			break
		}
		fileRecord, iterErr := resultSetIterator.Next()
		if iterErr != nil {
			return "", fmt.Errorf("Error iterating result: %s", iterErr)
		}
		var file FileAsset
		unmarshalErr := json.Unmarshal(fileRecord.Value, &file)
		if unmarshalErr != nil {
			return "", fmt.Errorf("Error iterating result: %s", unmarshalErr)
		}
		list.List = append(list.List, FileListRecord{file.IpfsHash, file.Name, file.Format, file.OwnerEmailID, file.Permissions})
	}
	listAsBytes, marshalErr := json.Marshal(list)
	if marshalErr != nil {
		return "", fmt.Errorf("Error unmarshaling files: %s", marshalErr)
	}
	return string(listAsBytes), nil
}

func getFileListByQuery(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	query := "{\"selector\":{"
	if args[0] != "" {
		query += fmt.Sprintf("\"ownerEmailId\":\"%s\"", args[0])
		if args[1] != "" {
			query += ","
		}
	}
	if args[1] != "" {
		query += fmt.Sprintf("\"permissions\":{\"$elemMatch\":{\"receiverEmailId\":\"%s\"}}", args[1])
	}
	query += "}}"
	println(query)

	resultSetIterator, queryErr := stub.GetQueryResult(query)
	if queryErr != nil {
		return "", fmt.Errorf("Error querying files: %s : ", queryErr)
	}
	return getFileListFromResultSet(resultSetIterator)
}

func getFileList(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 2 {
		return "", fmt.Errorf("Expected 2 arguments. Got %d", len(args))
	}
	if args[0] == "" && args[1] == "" {
		resultSetIterator, queryErr := stub.GetStateByRange("", "")
		if queryErr != nil {
			return "", fmt.Errorf("Error querying files: %s : ", queryErr)
		}
		return getFileListFromResultSet(resultSetIterator)
	}
	return getFileListByQuery(stub, args)
}

func main() {
	if err := shim.Start(new(FileAssetHandlers)); err != nil {
		fmt.Printf("Error starting FileAssetHandlers chaincode: %s", err)
	}
}
