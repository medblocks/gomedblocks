module gitlab.com/medblocks/medblockshyperledgerchaincode/assetFunctions

go 1.12

require (
	github.com/Knetic/govaluate v3.0.0+incompatible // indirect
	github.com/fsouza/go-dockerclient v1.4.2 // indirect
	github.com/go-kit/kit v0.9.0 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/hyperledger/fabric v0.0.0-20181030160221-60f968db8e6e
	github.com/hyperledger/fabric-amcl v0.0.0-20181230093703-5ccba6eab8d6 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7 // indirect
	github.com/pkg/errors v0.8.1
	github.com/spf13/viper v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586 // indirect
	google.golang.org/genproto v0.0.0-20180831171423-11092d34479b // indirect
	google.golang.org/grpc v1.23.0 // indirect
)
