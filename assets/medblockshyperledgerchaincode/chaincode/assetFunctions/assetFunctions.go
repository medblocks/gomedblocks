package assetFunctions

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"

	//	"math/big"
	b64 "encoding/base64"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

// Asset implements the chaincode to handle assets
type Asset struct {
}

//Structures to represent assets and JSON objects received

type SignedRequest struct {
	Data      string
	Signature string
}

type UserAsset struct {
	SPublicKey       string `json:"sPublicKey"`
	EPublicKey       string `json:"ePublicKey"`
	EmailID          string `json:"emailId"`
	IdentityFileHash string `json:"IdentityFileHash,omitempty`
	Metadata         string `json:"metadata,omitempty"`
}

func (t *Asset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func (t *Asset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	fn, args := stub.GetFunctionAndParameters()
	var result string
	var err error
	if fn == "newAsset" {
		result, err = newAsset(stub, args)
	} else if fn == "addMinters" {
		result, err = addMinters(stub, args)
	} else if fn == "removeMinters" {
		result, err = removeMinters(stub, args)
	} else if fn == "getMinters" {
		result, err = getMinters(stub, args)
	} else if fn == "changeAdmin" {
		result, err = changeAdmin(stub, args)
	} else if fn == "mint" {
		result, err = mint(stub, args)
	} else if fn == "burn" {
		result, err = burn(stub, args)
	} else if fn == "totalSupply" {
		result, err = totalSupply(stub, args)
	} else if fn == "balanceOf" {
		result, err = balanceOf(stub, args)
	} else if fn == "balanceOfAdmin" {
		result, err = balanceOfAdmin(stub, args)
	} else if fn == "transfer" {
		result, err = transfer(stub, args)
	} else if fn == "transferToAdmin" {
		result, err = transferToAdmin(stub, args)
	} else if fn == "transferFromAdmin" {
		result, err = transferFromAdmin(stub, args)
	} else {
		return shim.Error(fmt.Sprint("Unknown function: ", fn))
	}

	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println(result)

	return shim.Success([]byte(result))
}
func toChaincodeArgs(args ...string) [][]byte {
	bargs := make([][]byte, len(args))
	for i, arg := range args {
		bargs[i] = []byte(arg)
	}
	return bargs
}
func checkUserExists(stub shim.ChaincodeStubInterface, email string) bool {
	chaincodeArgs := toChaincodeArgs("queryUser", email)
	queryUserResponse := stub.InvokeChaincode("userF", chaincodeArgs, stub.GetChannelID())
	if queryUserResponse.GetPayload() != nil {
		return true
	}
	return false
}
func checkAssetExists(stub shim.ChaincodeStubInterface, AssetName string) bool {
	key, _ := stub.CreateCompositeKey(AssetName, []string{"Exists"})
	vaa, _ := stub.GetState(key)
	if vaa != nil {
		fmt.Println(vaa)
		return true
	}
	return false
}

/* verifySignature: verify if the message is signed by user with ownerEmailID */
func verifySignature(stub shim.ChaincodeStubInterface, ownerEmailID string, signature string, message string) error {
	chaincodeArgs := toChaincodeArgs("queryUser", ownerEmailID)
	queryUserResponse := stub.InvokeChaincode("userF", chaincodeArgs, stub.GetChannelID())
	if queryUserResponse.Status != shim.OK {
		return fmt.Errorf(fmt.Sprint("Error requesting file owner details: %s", queryUserResponse.Message))
	}
	userAsBytes := queryUserResponse.GetPayload()
	var user UserAsset
	err := json.Unmarshal([]byte(userAsBytes), &user)
	if err != nil {
		return fmt.Errorf(fmt.Sprint("Unmarshalling error: %s", err))
	}
	fmt.Println(user.SPublicKey)
	block, _ := pem.Decode([]byte(user.SPublicKey))
	if block == nil {
		return fmt.Errorf(fmt.Sprint("failed to decode PEM block containing public key"))
	}
	pubKey, parseErr := x509.ParsePKIXPublicKey(block.Bytes)
	if parseErr != nil {
		return fmt.Errorf(fmt.Sprint("Failed to parse public key: %s", parseErr))
	}
	rsaPubKey := pubKey.(*rsa.PublicKey)
	h := sha256.New()
	h.Write([]byte(message))
	digest := h.Sum(nil)
	decodedSignatureBytes, _ := b64.StdEncoding.DecodeString(signature)
	verifyErr := rsa.VerifyPKCS1v15(rsaPubKey, crypto.SHA256, digest, decodedSignatureBytes)
	return verifyErr
}
func getBalance(stub shim.ChaincodeStubInterface, key string) int {
	type Balance struct {
		Balance int `json:"balance"`
	}
	var balance Balance
	balanceBytes, err := stub.GetState(key)
	if err != nil {
		return 0
	}
	json.Unmarshal(balanceBytes, &(balance.Balance))
	return balance.Balance
}

func newAsset(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName      string   `json:"assetName"`
		AssetAdmin     string   `json:"assetAdmin"`
		AssetMinters   []string `json:"assetMinters"`
		InitialBalance int      `json:"balance"`
		TotalSupply    int      `json:"totalSupply"`
	}
	var req SignedRequest
	var param Params

	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.AssetAdmin == "" || param.AssetMinters == nil {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.AssetAdmin == "" {
			missing += " assetAdmin"
		}
		if param.AssetMinters == nil {
			missing += " assetMinters"
		}
		return "", fmt.Errorf(missing)
	}
	if checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset already exists: ", param.AssetName))
	}
	if !checkUserExists(stub, param.AssetAdmin) {
		return "", fmt.Errorf(fmt.Sprint("Admin does not exist: ", param.AssetAdmin))
	}
	minters := make(map[string]bool)
	for _, minter := range param.AssetMinters {
		minters[minter] = true
	}
	param.AssetMinters = nil
	for minter, _ := range minters {
		if !checkUserExists(stub, minter) {
			return "", fmt.Errorf(fmt.Sprint("Minter does not exist: ", minter))
		}
		param.AssetMinters = append(param.AssetMinters, minter)
	}

	if param.InitialBalance < 0 {
		return "", fmt.Errorf(fmt.Sprint("Initial Balance less than 0"))
	}
	err = verifySignature(stub, param.AssetAdmin, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.AssetAdmin))
	}

	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Exists"})
	stub.PutState(key, []byte{'1'})

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"Admin"})
	value, _ := json.Marshal(param.AssetAdmin)
	stub.PutState(key, value)

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"Minters"})
	value, _ = json.Marshal(param.AssetMinters)
	stub.PutState(key, value)

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"AdminBalance"})
	value, _ = json.Marshal(param.InitialBalance)
	stub.PutState(key, value)

	param.TotalSupply = param.InitialBalance

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"TotalSupply"})
	value, _ = json.Marshal(param.TotalSupply)
	stub.PutState(key, value)

	return "Created!", nil
}
func addMinters(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetAdmin   string   `json:"assetAdmin"`
		AssetName    string   `json:"assetName"`
		Minters      []string `json:"minters"`
		AssetMinters []string `json:"assetMinters"`
	}
	var req SignedRequest
	var param Params

	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.Minters == nil {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.Minters == nil {
			missing += " minters"
		}
		return "", fmt.Errorf(fmt.Sprint(missing))
	}

	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}

	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Admin"})
	adminBytes, _ := stub.GetState(key)
	json.Unmarshal(adminBytes, &(param.AssetAdmin))
	for _, minter := range param.Minters {
		if !checkUserExists(stub, minter) {
			return "", fmt.Errorf(fmt.Sprint("Minter does not exist: ", minter))
		}
	}
	err = verifySignature(stub, param.AssetAdmin, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.AssetAdmin))
	}

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"Minters"})
	currentMinterBytes, _ := stub.GetState(key)
	json.Unmarshal(currentMinterBytes, &(param.AssetMinters))

	Map := make(map[string]bool)

	for _, minter := range param.Minters {
		Map[minter] = true
	}
	for _, minter := range param.AssetMinters {
		Map[minter] = true
	}
	param.AssetMinters = []string{}
	for minter, _ := range Map {
		param.AssetMinters = append(param.AssetMinters, minter)
	}

	value, _ := json.Marshal(param.AssetMinters)
	stub.PutState(key, value)
	return string(value), nil
}
func removeMinters(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetAdmin   string   `json:"assetAdmin"`
		AssetName    string   `json:"assetName"`
		Minters      []string `json:"minters"`
		AssetMinters []string `json:"assetMinters"`
	}
	var req SignedRequest
	var param Params

	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.Minters == nil {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.Minters == nil {
			missing += " minters"
		}
		return "", fmt.Errorf(fmt.Sprint(missing))
	}
	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}
	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Admin"})
	adminBytes, _ := stub.GetState(key)
	json.Unmarshal(adminBytes, &(param.AssetAdmin))
	for _, minter := range param.Minters {
		if !checkUserExists(stub, minter) {
			return "", fmt.Errorf(fmt.Sprint("Minter does not exist: ", minter))
		}
	}
	err = verifySignature(stub, param.AssetAdmin, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.AssetAdmin))
	}

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"Minters"})
	currentMinterBytes, _ := stub.GetState(key)
	json.Unmarshal(currentMinterBytes, &(param.AssetMinters))

	Map := make(map[string]bool)

	for _, minter := range param.AssetMinters {
		Map[minter] = true
	}
	for _, minter := range param.Minters {
		Map[minter] = false
	}
	param.AssetMinters = []string{}
	for minter, v := range Map {
		if v {
			param.AssetMinters = append(param.AssetMinters, minter)
		}
	}
	if param.AssetMinters == nil {
		return "", fmt.Errorf(fmt.Sprint("Operation leaves no valid minters"))
	}
	value, _ := json.Marshal(param.AssetMinters)
	stub.PutState(key, value)
	return string(value), nil
}
func getMinters(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName    string   `json:"assetName"`
		AssetMinters []string `json:"assetMinters"`
	}
	var param Params
	if len(args) != 1 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 1 argument got %d", len(args)))
	}
	param.AssetName = args[0]
	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}
	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Minters"})
	currentMinterBytes, _ := stub.GetState(key)
	return string(currentMinterBytes), nil
}
func changeAdmin(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetAdmin string `json:"assetAdmin"`
		AssetName  string `json:"assetName"`
		NewAdmin   string `json:"newAdmin"`
	}
	var req SignedRequest
	var param Params

	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.NewAdmin == "" {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.NewAdmin == "" {
			missing += " newAdmin"
		}
		return "", fmt.Errorf(fmt.Sprint(missing))
	}

	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}
	if !checkUserExists(stub, param.NewAdmin) {
		return "", fmt.Errorf(fmt.Sprint("Admin Candidate does not exist: ", param.NewAdmin))
	}
	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Admin"})
	adminBytes, _ := stub.GetState(key)
	json.Unmarshal(adminBytes, &(param.AssetAdmin))
	err = verifySignature(stub, param.AssetAdmin, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.AssetAdmin))
	}
	param.AssetAdmin = param.NewAdmin
	value, _ := json.Marshal(param.AssetAdmin)
	stub.PutState(key, value)
	return string(value), nil
}

func mint(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName    string   `json:"assetName"`
		Minter       string   `json:"minter"`
		AssetMinters []string `json:"assetMinters"`
		Target       string   `json:"target"`
		Amount       int      `json:"amount"`
		Balance      int      `json:"balance"`
		TotalSupply  int      `json:"totalSupply"`
	}
	var req SignedRequest
	var param Params
	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.Minter == "" || param.Target == "" || param.Amount <= 0 {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.Minter == "" {
			missing += " minter"
		}
		if param.Target == "" {
			missing += " target"
		}
		if param.Amount <= 0 {
			missing += " amount"
		}
		return "", fmt.Errorf(fmt.Sprint(missing))
	}

	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}

	if !checkUserExists(stub, param.Target) {
		return "", fmt.Errorf(fmt.Sprint("Target does not exist: ", param.Target))
	}

	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Minters"})
	currentMinterBytes, _ := stub.GetState(key)
	json.Unmarshal(currentMinterBytes, &(param.AssetMinters))

	isValid := false
	for _, minter := range param.AssetMinters {
		if param.Minter == minter {
			isValid = true
			break
		}
	}
	if !isValid {
		return "", fmt.Errorf(fmt.Sprint("Unauthorized minter: ", param.Minter))
	}
	err = verifySignature(stub, param.Minter, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.Minter))
	}

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"TotalSupply"})
	supplyBytes, _ := stub.GetState(key)
	json.Unmarshal(supplyBytes, &param.TotalSupply)

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"Balance", param.Target})
	param.Balance = getBalance(stub, key)

	param.Balance = param.Balance + param.Amount
	param.TotalSupply = param.TotalSupply + param.Amount

	value, _ := json.Marshal(param.Balance)
	stub.PutState(key, value)

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"TotalSupply"})
	value, _ = json.Marshal(param.TotalSupply)
	stub.PutState(key, value)

	return string(value), nil
}

func burn(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetAdmin  string `json:"assetAdmin"`
		AssetName   string `json:"assetName"`
		Amount      int    `json:"amount"`
		Balance     int    `json:"balance"`
		TotalSupply int    `json:"totalSupply"`
	}
	var req SignedRequest
	var param Params
	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.Amount <= 0 {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.Amount <= 0 {
			missing += " amount"
		}
		return "", fmt.Errorf(fmt.Sprint(missing))
	}

	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}

	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Admin"})
	adminBytes, _ := stub.GetState(key)
	json.Unmarshal(adminBytes, &(param.AssetAdmin))

	err = verifySignature(stub, param.AssetAdmin, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.AssetAdmin))
	}

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"TotalSupply"})
	supplyBytes, _ := stub.GetState(key)
	json.Unmarshal(supplyBytes, &param.TotalSupply)

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"AdminBalance"})
	param.Balance = getBalance(stub, key)

	if param.Balance < param.Amount {
		param.Amount = param.Balance
	}

	param.Balance = param.Balance - param.Amount
	param.TotalSupply = param.TotalSupply - param.Amount

	value, _ := json.Marshal(param.Balance)
	stub.PutState(key, value)

	key, _ = stub.CreateCompositeKey(param.AssetName, []string{"TotalSupply"})
	value, _ = json.Marshal(param.TotalSupply)
	stub.PutState(key, value)

	value, _ = json.Marshal(param.Amount)
	return string(value), nil
}

func totalSupply(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName string `json:"assetName"`
	}
	var param Params
	if len(args) != 1 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 1 argument got %d", len(args)))
	}
	param.AssetName = args[0]

	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}
	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"TotalSupply"})
	supplyBytes, _ := stub.GetState(key)
	return string(supplyBytes), nil
}
func balanceOf(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName string `json:"assetName"`
		Target    string `json:"target"`
		Balance   int    `json:"balance`
	}
	var param Params
	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 argument got %d", len(args)))
	}
	param.AssetName = args[0]
	param.Target = args[1]

	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}
	if !checkUserExists(stub, param.Target) {
		return "", fmt.Errorf(fmt.Sprint("User does not exist: ", param.Target))
	}
	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Balance", param.Target})
	param.Balance = getBalance(stub, key)
	balanceBytes, _ := json.Marshal(param.Balance)
	return string(balanceBytes), nil
}
func balanceOfAdmin(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName string `json:"assetName"`
		Balance   int    `json:"balance`
	}
	var param Params
	if len(args) != 1 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 1 argument got %d", len(args)))
	}
	param.AssetName = args[0]

	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}
	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"AdminBalance"})
	param.Balance = getBalance(stub, key)
	balanceBytes, _ := json.Marshal(param.Balance)
	return string(balanceBytes), nil
}

func transfer(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName   string `json:"assetName"`
		From        string `json:"from"`
		To          string `json:"to"`
		Amount      int    `json:"amount"`
		FromBalance int    `json:"balance"`
		ToBalance   int    `json:"balance"`
	}
	var req SignedRequest
	var param Params
	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.From == "" || param.To == "" || param.Amount <= 0 {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.From == "" {
			missing += " from"
		}
		if param.To == "" {
			missing += " to"
		}
		if param.Amount <= 0 {
			missing += " amount"
		}
		return "", fmt.Errorf(fmt.Sprint(missing))
	}
	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}

	if !checkUserExists(stub, param.From) {
		return "", fmt.Errorf(fmt.Sprint("Source account does not exist: ", param.AssetName))
	}
	if !checkUserExists(stub, param.To) {
		return "", fmt.Errorf(fmt.Sprint("Destination account does not exist: ", param.AssetName))
	}
	if param.From == param.To {
		return "", fmt.Errorf("Cannot transfer to source account!")
	}

	err = verifySignature(stub, param.From, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.From))
	}

	keyFrom, _ := stub.CreateCompositeKey(param.AssetName, []string{"Balance", param.From})
	param.FromBalance = getBalance(stub, keyFrom)

	keyTo, _ := stub.CreateCompositeKey(param.AssetName, []string{"Balance", param.To})
	param.ToBalance = getBalance(stub, keyTo)

	if param.FromBalance < param.Amount {
		return "", fmt.Errorf(fmt.Sprint("Insufficient Balance: ", param.From, ", ", param.Amount))
	}

	param.FromBalance = param.FromBalance - param.Amount
	param.ToBalance = param.ToBalance + param.Amount

	value, _ := json.Marshal(param.FromBalance)
	stub.PutState(keyFrom, value)

	value, _ = json.Marshal(param.ToBalance)
	stub.PutState(keyTo, value)

	value, _ = json.Marshal(param.Amount)
	return string(value), nil
}
func transferToAdmin(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName   string `json:"assetName"`
		From        string `json:"from"`
		Amount      int    `json:"amount"`
		FromBalance int    `json:"balance"`
		ToBalance   int    `json:"balance"`
	}
	var req SignedRequest
	var param Params
	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.From == "" || param.Amount <= 0 {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.From == "" {
			missing += " from"
		}
		if param.Amount <= 0 {
			missing += " amount"
		}
		return "", fmt.Errorf(fmt.Sprint(missing))
	}
	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}

	if !checkUserExists(stub, param.From) {
		return "", fmt.Errorf(fmt.Sprint("Source account does not exist: ", param.AssetName))
	}

	err = verifySignature(stub, param.From, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.From))
	}

	keyFrom, _ := stub.CreateCompositeKey(param.AssetName, []string{"Balance", param.From})
	param.FromBalance = getBalance(stub, keyFrom)

	keyTo, _ := stub.CreateCompositeKey(param.AssetName, []string{"AdminBalance"})
	param.ToBalance = getBalance(stub, keyTo)

	if param.FromBalance < param.Amount {
		return "", fmt.Errorf(fmt.Sprint("Insufficient Balance: ", param.From, ", ", param.Amount))
	}

	param.FromBalance = param.FromBalance - param.Amount
	param.ToBalance = param.ToBalance + param.Amount

	value, _ := json.Marshal(param.FromBalance)
	stub.PutState(keyFrom, value)

	value, _ = json.Marshal(param.ToBalance)
	stub.PutState(keyTo, value)

	value, _ = json.Marshal(param.Amount)
	return string(value), nil
}
func transferFromAdmin(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	type Params struct {
		AssetName   string `json:"assetName"`
		AssetAdmin  string `json:"assetAdmin"`
		To          string `json:"to"`
		Amount      int    `json:"amount"`
		FromBalance int    `json:"balance"`
		ToBalance   int    `json:"balance"`
	}
	var req SignedRequest
	var param Params
	if len(args) != 2 {
		return "", fmt.Errorf(fmt.Sprint("Incorrect number of arguments. Expected 2 arguments got %d", len(args)))
	}

	req.Data = args[0]
	req.Signature = args[1]

	err := json.Unmarshal([]byte(req.Data), &param)
	if err != nil {
		fmt.Println(req.Data)
		return "", err
	}
	fmt.Println(param)
	if param.AssetName == "" || param.To == "" || param.Amount <= 0 {
		missing := "Missing Required fields:"
		if param.AssetName == "" {
			missing += " assetName"
		}
		if param.To == "" {
			missing += " to"
		}
		if param.Amount <= 0 {
			missing += " amount"
		}
		return "", fmt.Errorf(fmt.Sprint(missing))
	}
	if !checkAssetExists(stub, param.AssetName) {
		return "", fmt.Errorf(fmt.Sprint("Asset does not exist: ", param.AssetName))
	}

	if !checkUserExists(stub, param.To) {
		return "", fmt.Errorf(fmt.Sprint("Destination account does not exist: ", param.To))
	}

	key, _ := stub.CreateCompositeKey(param.AssetName, []string{"Admin"})
	adminBytes, _ := stub.GetState(key)
	json.Unmarshal(adminBytes, &(param.AssetAdmin))

	err = verifySignature(stub, param.AssetAdmin, req.Signature, req.Data)
	if err != nil {
		return "", fmt.Errorf(fmt.Sprint("Signature Verification Failed: ", param.AssetName, " ", param.AssetAdmin))
	}

	keyTo, _ := stub.CreateCompositeKey(param.AssetName, []string{"Balance", param.To})
	param.ToBalance = getBalance(stub, keyTo)
	keyFrom, _ := stub.CreateCompositeKey(param.AssetName, []string{"AdminBalance"})
	param.FromBalance = getBalance(stub, keyFrom)

	if param.FromBalance < param.Amount {
		return "", fmt.Errorf(fmt.Sprint("Insufficient Balance: ", param.AssetAdmin, ", ", param.Amount))
	}

	param.FromBalance = param.FromBalance - param.Amount
	param.ToBalance = param.ToBalance + param.Amount

	value, _ := json.Marshal(param.FromBalance)
	stub.PutState(keyFrom, value)

	value, _ = json.Marshal(param.ToBalance)
	stub.PutState(keyTo, value)

	value, _ = json.Marshal(param.Amount)
	return string(value), nil
}
