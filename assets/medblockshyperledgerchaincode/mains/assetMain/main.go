package main

import (
	"fmt"

	asset "gitlab.com/medblocks/chaincode/assetFunctions"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {
	if err := shim.Start(new(asset.Asset)); err != nil {
		fmt.Printf("Error starting FileAssetHandlers chaincode: %s", err)
	}
}
