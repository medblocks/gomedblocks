package main

import (
	"fmt"

	userF "gitlab.com/medblocks/chaincode/userFunctions"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {

	// Create a new Smart Contract
	err := shim.Start(new(userF.RegisterUserSmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
