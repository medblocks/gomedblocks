package main

import (
	"fmt"

	fileF "gitlab.com/medblocks/chaincode/FileAssetHandlers"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func main() {
	if err := shim.Start(new(fileF.FileAssetHandlers)); err != nil {
		fmt.Printf("Error starting FileAssetHandlers chaincode: %s", err)
	}
}
