usage()
{
    echo "Usage:"
    echo "createOrg.sh -o ORG_NAME -cp CA_PORT -t TLS_ROOT_CERT_PATH [FLAGS]"
    echo "Available FLAGS:"
    echo " -o or --org_name                 Name of the organization being added"
    echo " -t or --tls_root_cert_path       path to trusted root cert of the TLS CA"
    echo " -m or --msp_name                 Name of the organization MSP (Default {org_name}MSP)"
    echo " -cp or --ca_port                 Port at which you want to host the org CA (currently only at addresss 0.0.0.0)"
    echo " -cu or --ca_username             admin username for the ORG CA (Default admin)"
    echo " -cpw or --ca_password            admin password for the ORG CA (Default password)"
    echo " -ou or --org_username            admin username for the ORG (Default admin)"
    echo " -opw or --org_password           admin password for the ORG (Default password)"
    echo " -An or --anchor_peer_name        Name of the initial anchor peer created for org (Default peer1)"
    echo " -Aa or --anchor_peer_address     Address of the initial anchor peer created for org (Default set to {anchor_peer_name}-{org_name})"
    echo " -Ap or --anchor_peer_port        Listen port of the initial anchor peer created for org (Default 7051)"
}

org_name=""
tls_root_cert_path=
msp_name=
ca_port=""
ca_username=admin
ca_password=password
org_username=admin
org_password=password
anchor_peer_name=peer1-org
anchor_peer_address=
anchor_peer_port=7051

while [ "$1" != "" ]; do
    case $1 in
        -o | --org_name )               shift
                                        org_name=$1
                                        ;;
        -m | --msp_name )               shift
                                        msp_name=$1
                                        ;;
        -cp | --ca_port )               shift
                                        ca_port=$1
                                        ;;
        -cu | --ca_username )           shift
                                        ca_username=$1
                                        ;;
        -cpw | --ca_password )          shift
                                        ca_password=$1
                                        ;;
        -ou | --org_username )           shift
                                        org_username=$1
                                        ;;
        -opw | --org_password )          shift
                                        org_password=$1
                                        ;;
        -An | --anchor_peer_name )      shift
                                        anchor_peer_name=$1
                                        ;;
        -Aa | --anchor_peer_address )   shift
                                        anchor_peer_address=$1
                                        ;;
        -Ap | --anchor_peer_port )      shift
                                        anchor_peer_port=$1
                                        ;;
        -t | --tls_root_cert_path )     shift
                                        tls_root_cert_path=$1
                                        ;;
        -h | --help )                   usage
                                        exit
                                        ;;
        * )                             usage
                                        exit 1
    esac
    shift
done

echo $org_name
echo $ca_port

if [ -z "$org_name" ] || [ -z "$ca_port" ] || [ -z "$tls_root_cert_path" ]
then
    echo "Required arguments not provided."
    usage
    exit 1
fi

if [ -z "$msp_name" ]
then
    msp_name="${org_name}MSP"
fi

if [ -z "$anchor_peer_address" ]
then
    anchor_peer_address=${anchor_peer_name}-${org_name}
fi

sudo mkdir -p /tmp/hyperledger/${org_name}/ca/crypto/
sudo mkdir -p /tmp/hyperledger/${org_name}/tmp/
sudo mkdir -p docker/$org_name

scripts/orgMaintainer/genOrgCA.sh -o ${org_name} -cp ${ca_port} -cpw ${ca_password} -cu ${ca_username}
scripts/orgMaintainer/genOrgCLI.sh -o ${org_name} -m ${msp_name} -Aa ${anchor_peer_address} -An ${anchor_peer_name} -Ap ${anchor_peer_port}

docker-compose -f docker/${org_name}/${org_name}-ca-docker-compose.yaml up -d
sleep 1
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/${org_name}/ca/crypto/ca-cert.pem
export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/${org_name}/ca/admin

fabric-ca-client enroll -d -u https://${ca_username}:${ca_password}@0.0.0.0:${ca_port}
fabric-ca-client affiliation add ${org_name} -u https://0.0.0.0:${ca_port}
fabric-ca-client register -d --id.name ${org_username}   --id.affiliation ${org_name} --id.secret ${org_password}     --id.type admin -u https://0.0.0.0:${ca_port}  --id.attrs '"hf.Registrar.Roles=peer,oracle,client"'

export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/${org_name}/${org_username}
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/${org_name}/ca/crypto/ca-cert.pem
export FABRIC_CA_CLIENT_MSPDIR=msp
fabric-ca-client enroll -d -u https://${org_username}:${org_password}@0.0.0.0:${ca_port}

sudo mkdir -p /tmp/hyperledger/${org_name}/msp/admincerts/
sudo mkdir -p /tmp/hyperledger/${org_name}/msp/cacerts/
sudo mkdir -p /tmp/hyperledger/${org_name}/msp/tlscacerts/
sudo mkdir -p /tmp/hyperledger/${org_name}/msp/users/
sudo mkdir -p /tmp/hyperledger/${org_name}/${org_username}/msp/admincerts/

sudo cp /tmp/hyperledger/${org_name}/${org_username}/msp/signcerts/cert.pem                 /tmp/hyperledger/${org_name}/msp/admincerts/${org_username}-cert.pem
sudo cp /tmp/hyperledger/${org_name}/${org_username}/msp/signcerts/cert.pem                 /tmp/hyperledger/${org_name}/${org_username}/msp/admincerts/${org_username}-cert.pem
sudo cp /tmp/hyperledger/${org_name}/${org_username}/msp/cacerts/0-0-0-0-${ca_port}.pem     /tmp/hyperledger/${org_name}/msp/cacerts/${org_name}-ca-cert.pem
sudo cp ${tls_root_cert_path}                                                               /tmp/hyperledger/${org_name}/msp/tlscacerts/tls-ca-cert.pem

docker-compose -f docker/${org_name}/cli-docker-compose.yaml up -d