usage()
{
    echo "Usage:"
    echo "genOrgCLI.sh -o ORG_NAME [FLAGS]"
    echo "Available FLAGS:"
    echo " -o or --org_name                 Name of the organization being added"
    echo " -m or --msp_name                 Name of the organization MSP (Default {org_name}MSP)"
    echo " -An or --anchor_peer_name        Name of the initial anchor peer created for org (Default peer1-org)"
    echo " -Aa or --anchor_peer_address     Address of the initial anchor peer created for org (Default set to anchor_peer_name)"
    echo " -Ap or --anchor_peer_port        Listen port of the initial anchor peer created for org (Default 7051)"
}

org_name=""
msp_name=
anchor_peer_name=peer1-org
anchor_peer_address=
anchor_peer_port=7051

while [ "$1" != "" ]; do
    case $1 in
        -o | --org_name )               shift
                                        org_name=$1
                                        ;;
        -m | --msp_name )               shift
                                        msp_name=$1
                                        ;;
        -An | --anchor_peer_name )      shift
                                        anchor_peer_name=$1
                                        ;;
        -Aa | --anchor_peer_address )   shift
                                        anchor_peer_address=$1
                                        ;;
        -Ap | --anchor_peer_port )      shift
                                        anchor_peer_port=$1
                                        ;;
        -h | --help )                   usage
                                        exit
                                        ;;
        * )                             usage
                                        exit 1
    esac
    shift
done

echo $org_name
echo $ca_port

if [ -z "$org_name" ]
then
    echo "Required arguments not provided."
    usage
    exit 1
fi

if [ -z "$msp_name" ]
then
    msp_name="${org_name}MSP"
fi

if [ -z "$anchor_peer_address" ]
then
    anchor_peer_address=$anchor_peer_name
fi


cp docker/org-template/cli-docker-compose.yaml  docker/${org_name}/cli-docker-compose.yaml
bash -c "sed -i 's/{ORGNAME}/${org_name}/g' docker/${org_name}/cli-docker-compose.yaml"
bash -c "sed -i 's/{ORGMSP}/${msp_name}/g' docker/${org_name}/cli-docker-compose.yaml"
bash -c "sed -i 's/{PEERNAME}/${anchor_peer_name}/g' docker/${org_name}/cli-docker-compose.yaml"
bash -c "sed -i 's/{PEERADDRESS}/${anchor_peer_address}:${anchor_peer_port}/g' docker/${org_name}/cli-docker-compose.yaml"