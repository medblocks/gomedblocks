usage()
{
    echo "Usage:"
    echo "add_org.sh -o ORG_NAME -cp CA_PORT [FLAGS]"
    echo "Available FLAGS:"
    echo " -o or --org_name                 Name of the organization being added"
    echo " -m or --msp_name                 Name of the organization MSP (Default {org_name}MSP)"
    echo " -C or --channel_name             Channel name to join org to (Default mychannel)"
}

org_name=""
msp_name=
channel_name=mychannel

while [ "$1" != "" ]; do
    case $1 in
        -o | --org_name )               shift
                                        org_name=$1
                                        ;;
        -m | --msp_name )               shift
                                        msp_name=$1
                                        ;;
        -C | --channel_name )           shift
                                        channel_name=$1
                                        ;;
        -h | --help )                   usage
                                        exit
                                        ;;
        * )                             usage
                                        exit 1
    esac
    shift
done

if [ -z "$org_name" ]
then
    echo "Required arguments not provided."
    usage
    exit 1
fi

if [ -z "$msp_name" ]
then
    msp_name="${org_name}MSP"
fi

cd /tmp/hyperledger/${org_name}/tmp/
echo yo
configtxlator proto_decode --input config_block.pb --type common.Block | jq .data.data[0].payload.data.config > config.json
echo yo
jq -s ".[0] * {\"channel_group\":{\"groups\":{\"Application\":{\"groups\": {\"${msp_name}\":.[1]}}}}}" config.json ${org_name}.json > modified_config.json
echo yo
configtxlator proto_encode --input config.json --type common.Config --output config.pb
echo yo
configtxlator proto_encode --input modified_config.json --type common.Config --output modified_config.pb
echo yo
configtxlator compute_update --channel_id ${channel_name} --original config.pb --updated modified_config.pb --output ${org_name}_update.pb
echo yo
configtxlator proto_decode --input ${org_name}_update.pb --type common.ConfigUpdate | jq . > ${org_name}_update.json
echo "{\"payload\":{\"header\":{\"channel_header\":{\"channel_id\":\"${channel_name}\", \"type\":2}},\"data\":{\"config_update\":"$(cat ${org_name}_update.json)"}}}" | jq . > ${org_name}_update_in_envelope.json
configtxlator proto_encode --input ${org_name}_update_in_envelope.json --type common.Envelope --output ${org_name}_final_update.pb
cd -
