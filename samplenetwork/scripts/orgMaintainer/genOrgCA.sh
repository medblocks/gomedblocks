usage()
{
    echo "Usage:"
    echo "genOrgCA.sh -o ORG_NAME -cp CA_PORT [FLAGS]"
    echo "Available FLAGS:"
    echo " -o or --org_name                 Name of the organization being added"
    echo " -cp or --ca_port                 Port at which you want to host the org CA (currently only at addresss 0.0.0.0)"
    echo " -cu or --ca_username             admin username for the ORG CA (Default admin)"
    echo " -cpw or --ca_password            admin password for the ORG CA (Default password)"
}

org_name=""
ca_port=""
ca_username=
ca_password=
while [ "$1" != "" ]; do
    case $1 in
        -o | --org_name )               shift
                                        org_name=$1
                                        ;;
        -cp | --ca_port )               shift
                                        ca_port=$1
                                        ;;
        -cu | --ca_username )           shift
                                        ca_username=$1
                                        ;;
        -cpw | --ca_password )          shift
                                        ca_password=$1
                                        ;;
        -h | --help )                   usage
                                        exit
                                        ;;
        * )                             usage
                                        exit 1
    esac
    shift
done

echo $org_name
echo $ca_port

if [ -z "$org_name" ] || [ -z "$ca_port" ]
then
    echo "Required arguments not provided."
    usage
    exit 1
fi

if [ -z "$ca_username" ]
then
    ca_username="admin"
fi

if [ -z "$ca_password" ]
then
    ca_password="password"
fi


cp docker/org-template/default-ca-docker-compose.yaml  docker/${org_name}/${org_name}-ca-docker-compose.yaml
bash -c "sed -i 's/{ORGNAME}/${org_name}/g' docker/${org_name}/${org_name}-ca-docker-compose.yaml"
bash -c "sed -i 's/{CAPORT}/${ca_port}/g' docker/${org_name}/${org_name}-ca-docker-compose.yaml"
bash -c "sed -i 's/{CAUsername}/${ca_username}/g' docker/${org_name}/${org_name}-ca-docker-compose.yaml"
bash -c "sed -i 's/{CAPassword}/${ca_password}/g' docker/${org_name}/${org_name}-ca-docker-compose.yaml"
