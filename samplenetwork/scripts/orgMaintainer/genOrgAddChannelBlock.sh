usage()
{
    echo "Usage:"
    echo "genOrgAddChannelBlock.sh -o ORG_NAME -Aa ANCHOR_PEER_ADDRESS [FLAGS]"
    echo "Available FLAGS:"
    echo " -o or --org_name                 Name of the organization being added"
    echo " -m or --msp_name                 Name of the organization MSP (Default {org_name}MSP)"
    echo " -C or --channel_name             name of channel to join org to (Default mychannel)"
    echo " -Aa or --anchor_peer_address     Address of the initial anchor peer created for org"
    echo " -Ap or --anchor_peer_port        Listen port of the initial anchor peer created for org (Default 7051)"
}

org_name=""
msp_name=
anchor_peer_address=
anchor_peer_port=7051
channel_name=mychannel

while [ "$1" != "" ]; do
    case $1 in
        -o | --org_name )               shift
                                        org_name=$1
                                        ;;
        -m | --msp_name )               shift
                                        msp_name=$1
                                        ;;
        -C | --channel_name )           shift
                                        channel_name=$1
                                        ;;
        -Aa | --anchor_peer_address )   shift
                                        anchor_peer_address=$1
                                        ;;
        -Ap | --anchor_peer_port )      shift
                                        anchor_peer_port=$1
                                        ;;
        -h | --help )                   usage
                                        exit
                                        ;;
        * )                             usage
                                        exit 1
    esac
    shift
done

echo $org_name
echo $ca_port

if [ -z "$org_name" ] || [ -z "$anchor_peer_address" ]
then
    echo "Required arguments not provided."
    usage
    exit 1
fi

if [ -z "$msp_name" ]
then
    msp_name="${org_name}MSP"
fi

echo ya
sudo cp config/newOrgTemplate.yaml /tmp/hyperledger/${org_name}/tmp/configtx.yaml
bash -c "sed -i 's/{ORGNAME}/${org_name}/g' /tmp/hyperledger/${org_name}/tmp/configtx.yaml"
bash -c "sed -i 's/{MSPNAME}/${msp_name}/g' /tmp/hyperledger/${org_name}/tmp/configtx.yaml"
bash -c "sed -i 's/{PEERADDRESS}/${anchor_peer_address}/g' /tmp/hyperledger/${org_name}/tmp/configtx.yaml"
bash -c "sed -i 's/{PEERPORT}/${anchor_peer_port}/g' /tmp/hyperledger/${org_name}/tmp/configtx.yaml"

echo ya
export FABRIC_CFG_PATH=/tmp/hyperledger/${org_name}/tmp
bin/configtxgen -printOrg ${org_name} > /tmp/hyperledger/${org_name}/tmp/${org_name}.json

echo ya
docker exec -it cli-${org_name} bash -c "/tmp/hyperledger/scripts/addOrgChannel.sh -o ${org_name} -C ${channel_name} -m ${msp_name}"
echo ya