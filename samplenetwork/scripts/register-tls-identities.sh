# To be run inside the TLS CA server
#export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/tls-ca/crypto/tls-ca-cert.pem
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/tls/ca/crypto/ca-cert.pem
export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/tls/ca/admin
fabric-ca-client enroll -u https://tls-ca-admin:tls-ca-adminpw@0.0.0.0:7052
fabric-ca-client affiliation add medblocks -u https://0.0.0.0:7052
fabric-ca-client register --id.affiliation medblocks     --id.name peer1-medblocks --id.secret peer1PW --id.type peer -u https://0.0.0.0:7052 --id.attrs '"hf.Registrar.Roles=peer,admin,client"'
# fabric-ca-client register --id.affiliation indicare     --id.name peer2-indicare --id.secret peer2PW --id.type peer -u https://0.0.0.0:7052 --id.attrs '"hf.Registrar.Roles=peer,client"'
# fabric-ca-client register --id.affiliation sana         --id.name peer1-sana --id.secret peer1PW --id.type peer -u https://0.0.0.0:7052 --id.attrs '"hf.Registrar.Roles=peer,oracle,client"'
# fabric-ca-client register --id.affiliation sana         --id.name peer2-sana --id.secret peer2PW --id.type peer -u https://0.0.0.0:7052 --id.attrs '"hf.Registrar.Roles=peer,client"'
fabric-ca-client register --id.affiliation medblocks    --id.name orderer-medblocks --id.secret ordererPW --id.type orderer -u https://0.0.0.0:7052
# fabric-ca-client register --id.affiliation medblocks    --id.name user1-medblocks --id.secret ordererPW --id.type client -u https://0.0.0.0:7052
# fabric-ca-client register --id.affiliation medblocks    --id.name admin-medblocks --id.secret ordererPW --id.type admin -u https://0.0.0.0:7052
