usage()
{
    echo "Usage:"
    echo "add_org.sh -o ORG_NAME -cp CA_PORT -t TLS_ROOT_CERT_PATH [FLAGS]"
    echo "Available FLAGS:"
    echo " -o or --org_name                 Name of the organization being added"
    echo " -t or --tls_root_cert_path       path to trusted root cert of the TLS CA"
    echo " -m or --msp_name                 Name of the organization MSP (Default {org_name}MSP)"
    echo " -C or --channel_name             name of channel to join org to (Default mychannel)"
    echo " -cp or --ca_port                 Port at which you want to host the org CA (currently only at addresss 0.0.0.0)"
    echo " -cu or --ca_username             admin username for the ORG CA (Default admin)"
    echo " -cpw or --ca_password            admin password for the ORG CA (Default password)"
    echo " -ou or --org_username            admin username for the ORG (Default admin)"
    echo " -opw or --org_password           admin password for the ORG (Default password)"
    echo " -An or --anchor_peer_name        Name of the initial anchor peer created for org (Default peer1)"
    echo " -Aa or --anchor_peer_address     Address of the initial anchor peer created for org (Default set to {anchor_peer_name}-{org_name})"
    echo " -Ap or --anchor_peer_port        Listen port of the initial anchor peer created for org (Default 7051)"
}

org_name=""
tls_root_cert_path=""
msp_name=""
ca_port=
ca_username=admin
ca_password=password
org_username=admin
org_password=password
anchor_peer_name=peer1-org
anchor_peer_address=
anchor_peer_port=7051
channel_name=mychannel

while [ "$1" != "" ]; do
    case $1 in
        -o | --org_name )               shift
                                        org_name=$1
                                        ;;
        -m | --msp_name )               shift
                                        msp_name=$1
                                        ;;
        -C | --channel_name )           shift
                                        channel_name=$1
                                        ;;
        -cp | --ca_port )               shift
                                        ca_port=$1
                                        ;;
        -cu | --ca_username )           shift
                                        ca_username=$1
                                        ;;
        -cpw | --ca_password )          shift
                                        ca_password=$1
                                        ;;
        -ou | --org_username )           shift
                                        org_username=$1
                                        ;;
        -opw | --org_password )          shift
                                        org_password=$1
                                        ;;
        -An | --anchor_peer_name )      shift
                                        anchor_peer_name=$1
                                        ;;
        -Aa | --anchor_peer_address )   shift
                                        anchor_peer_address=$1
                                        ;;
        -Ap | --anchor_peer_port )      shift
                                        anchor_peer_port=$1
                                        ;;
        -t | --tls_root_cert_path )     shift
                                        tls_root_cert_path=$1
                                        ;;
        -h | --help )                   usage
                                        exit
                                        ;;
        * )                             usage
                                        exit 1
    esac
    shift
done

if [ -z "$org_name" ] || [ -z "$ca_port" ] || [ -z "$tls_root_cert_path" ]
then
    echo "Required arguments not provided."
    usage
    exit 1
fi

if [ -z "$msp_name" ]
then
    msp_name="${org_name}MSP"
fi

if [ -z "$anchor_peer_address" ]
then
    anchor_peer_address=${anchor_peer_name}-${org_name}
fi 

docker exec -it cli-medblocks bash -c "/tmp/hyperledger/scripts/fetchChannelBlocks.sh"
scripts/orgMaintainer/createOrg.sh -o ${org_name} -m ${msp_name} -cp ${ca_port} -cu ${ca_username} -cpw ${ca_password} -ou ${org_username} -opw ${org_password} -An ${anchor_peer_name} -Aa ${anchor_peer_address} -Ap ${anchor_peer_port} -t ${tls_root_cert_path}
sudo cp /tmp/hyperledger/medblocks/tmp/config_block.pb /tmp/hyperledger/${org_name}/tmp/config_block.pb
scripts/orgMaintainer/genOrgAddChannelBlock.sh -o ${org_name} -m ${msp_name} -Aa ${anchor_peer_address} -Ap ${anchor_peer_port} -C ${channel_name}

sudo cp /tmp/hyperledger/${org_name}/tmp/${org_name}_final_update.pb /tmp/hyperledger/medblocks/tmp/channel_update.pb
docker exec -it cli-medblocks bash -c "/tmp/hyperledger/scripts/signChannelUpdate.sh -C ${channel_name} -b /tmp/hyperledger/medblocks/tmp/channel_update.pb --update"
