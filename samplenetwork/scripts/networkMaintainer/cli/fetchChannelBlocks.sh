export ORDERER_CA=/tmp/hyperledger/medblocks/orderer/tls-msp/tlscacerts/tls-0-0-0-0-7052.pem
export CHANNEL_NAME=mychannel
cd /tmp/hyperledger/medblocks/tmp
peer channel fetch config config_block.pb -o orderer-medblocks:7050 -c $CHANNEL_NAME --tls --cafile $ORDERER_CA
cd -