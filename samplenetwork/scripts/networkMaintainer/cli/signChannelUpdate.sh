

usage()
{
    echo "Usage:"
    echo "signChannelUpdate.sh -b BLOCK_PATH -o ORG_NAME [FLAGS]"
    echo "Available FLAGS:"
    echo " -b or --block                    path to block to be signed"
    echo " -o or --org_name                 name of signing org"
    echo " -C or --channel_name             Channel name to join org to (Default mychannel)"
    echo " --update                         If this flag is set, block is sent to orderer after signing"
}

block=
channel_name=mychannel
update=false
org_name=
while [ "$1" != "" ]; do
    case $1 in
        -b | --block )                  shift
                                        block=$1
                                        ;;
        -C | --channel_name )           shift
                                        channel_name=$1
                                        ;;
        --update )                      
                                        update=true
                                        ;;
        -h | --help )                   usage
                                        exit
                                        ;;
        * )                             usage
                                        exit 1
    esac
    shift
done

if [ -z "$block" ]
then
    echo "Required arguments not provided."
    usage
    exit 1
fi

export ORDERER_CA=/tmp/hyperledger/medblocks/orderer/tls-msp/tlscacerts/tls-0-0-0-0-7052.pem

cd /tmp/hyperledger/medblocks/tmp/
if [ "$update" = false ]
then 
    CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer channel signconfigtx -f ${block}
else
    CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer channel update -f ${block} -c ${channel_name} -o orderer-medblocks:7050 --tls --cafile $ORDERER_CA
fi
cd -