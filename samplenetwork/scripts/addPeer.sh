#!/bin/bash
# Script to regiseter a new peer with an organization's CA and generate the peer container's docker compose file
# Usage:
# add_peer.sh -o ORG_NAME -p PEER_NAME -c CA_PORT

usage()
{
    echo "Usage:"
    echo "add_peer.sh -o ORG_NAME -p PEER_NAME -cp CA_PORT -t TLS_ROOT_CERT_PATH [FLAGS]"
    echo "Command line options:"
    echo " -o or --org_name                     Name of the organization"
    echo " -p or --peer_name                    Name of the peer"
    echo " -ca or --ca_address                  address at which org CA runs (Default 0.0.0.0)"
    echo " -cp or --ca_port                     port at which org CA runs"
    echo " -t or --tls_root_cert_path           path to trusted root cert of the TLS CA"
    echo " -plp or --peer_listen_port          Listen port of the peer (Default 7001)"
    echo " -pcp or --peer_chaincode_port       Chaincode port of the peer (Default 7002)"
    echo " -pgp or --peer_gossip_port           Gossip port of the peer (Default 7003)"
    echo " -gp or --gossip_peer_name            Name of the gossip bootstrap for the peer (Default peer1)"
    echo " -gpp or --gossip_peer_port           Gossip port of the bootstrap peer (Default same as peer_gossip_port)"
    echo " -m or --msp_name                     Name of the organization MSP (Default {org_name}MSP)"
    echo " -C or --channel_name                 name of channel to join peer to (Default mychannel)"
    echo " --anchor                             generate peer as an org's anchor peer"
    echo " --cli                                generate a CLI container along with the peer"
}

ca_port=
peer_name=
org_name=
tls_root_cert_path=

ca_address="0.0.0.0"
peer_listen_port=7001
peer_chaincode_port=7002
peer_gossip_port=7003
gossip_peer_name=peer1
gossip_peer_port=
msp_name=
channel_name=mychannel
anchor=0
cli=0

while [ "$1" != "" ]; do
    case $1 in
        -o | --org_name )       shift
                                org_name=$1
                                ;;
        -p | --peer_name )      shift
                                peer_name=$1
                                ;;
        -cp | --ca_port )        shift
                                ca_port=$1
                                ;;
        -t | --tls_root_cert_path )        shift
                                tls_root_cert_path=$1
                                ;;
        -ca | --ca_address )        shift
                                ca_address=$1
                                ;;
        -plp | --peer_listen_port )        shift
                                peer_listen_port=$1
                                ;;
        -pcp | --peer_chaincode_port )        shift
                                peer_chaincode_port=$1
                                ;;
        -pgp | --peer_gossip_port )        shift
                                peer_gossip_port=$1
                                ;;
        -gp | --gossip_peer_name )        shift
                                gossip_peer_name=$1
                                ;;
        -gpp | --gossip_peer_port )        shift
                                gossip_peer_porth=$1
                                ;;
        -m | --msp_name )        shift
                                msp_name=$1
                                ;;
        -C | --channel_name )        shift
                                channel_name=$1
                                ;;
        --anchor )
                                anchor=1
                                ;;
        --cli )
                                cli=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ -z "$org_name" ] || [ -z "$ca_port" ] || [ -z "$tls_root_cert_path" ] || [ -z "$peer_name" ]
then
    echo "Required arguments not provided."
    usage
    exit 1
fi

if [ -z "$msp_name" ]
then
    msp_name="${org_name}MSP"
fi
if [ -z "$gossip_peer_port" ]
then
    gossip_peer_port="${peer_gossip_port}"
fi

mkdir -p /tmp/hyperledger/$org_name/$peer_name/assets/ca/
mkdir -p /tmp/hyperledger/$org_name/$peer_name/assets/tls-ca/
cp /tmp/hyperledger/$org_name/ca/crypto/ca-cert.pem  /tmp/hyperledger/$org_name/$peer_name/assets/ca/${org_name}-ca-cert.pem
cp /tmp/hyperledger/tls/ca/crypto/tls-cert.pem      /tmp/hyperledger/$org_name/$peer_name/assets/tls-ca/tls-ca-cert.pem


echo "-------------------------- Registering ${org_name} identities --------------------------"
bash -c "ORG_NAME=$org_name PEER_NAME=$peer_name CA_PORT=$ca_port ./scripts/setup_peer.sh"

mkdir -p /tmp/hyperledger/$org_name/$peer_name/msp/admincerts/
cp /tmp/hyperledger/$org_name/admin/msp/signcerts/cert.pem /tmp/hyperledger/$org_name/$peer_name/msp/admincerts/${org_name}-admin-cert.pem
