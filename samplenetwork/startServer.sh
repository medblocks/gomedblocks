export IMAGE_TAG=latest

echo "Starting CA servers..."

docker-compose -f docker/ca-tls/ca-tls-docker-compose.yaml up -d
docker-compose -f docker/Medblocks/medblocks-ca-docker-compose.yaml up -d
docker-compose -f docker/Indicare/indicare-ca-docker-compose.yaml up -d
docker-compose -f docker/Sana/sana-ca-docker-compose.yaml up -d

echo "Started CA servers."
echo ""

echo "Starting orderer ..."
docker-compose -f docker/Medblocks/orderer-docker-compose.yaml up -d
echo "Starting peers..."

docker-compose -f docker/Indicare/peers-docker-compose.yaml up -d
docker-compose -f docker/Sana/peers-docker-compose.yaml up -d

echo "Starting cli ..."
docker-compose -f docker/Indicare/cli-docker-compose.yaml up -d
docker-compose -f docker/Sana/cli-docker-compose.yaml up -d
