export IMAGE_TAG=latest
sudo clear
sudo rm -R /tmp/hyperledger
docker-compose -f docker/ca-tls/ca-tls-docker-compose.yaml          down
docker-compose -f docker/Medblocks/medblocks-ca-docker-compose.yaml down
docker-compose -f docker/Sana/sana-ca-docker-compose.yaml           down
docker-compose -f docker/Medblocks/orderer-docker-compose.yaml      down
docker-compose -f docker/Medblocks/peers-docker-compose.yaml         down
# docker-compose -f docker/Sana/peers-docker-compose.yaml             down
docker-compose -f docker/Medblocks/cli-docker-compose.yaml           down
# docker-compose -f docker/Sana/cli-docker-compose.yaml               down
docker rm -f $(docker ps -a -q)
# docker volume prune -f
docker network prune -f
docker network create medblocks-network


git clone https://gitlab.com/medblocks/hyperledgerchaincode.git
cd hyperledgerchaincode
git pull
cd ..

echo "----------------------------- Starting TLS-CA container -----------------------------"
docker-compose -f docker/ca-tls/ca-tls-docker-compose.yaml -p "" up -d 
sleep 1
echo "------------------- Enrolling TLS-CA admin and register identities ------------------"
scripts/register-tls-identities.sh

echo "--------------------- Copying trusted root certificate to peers ---------------------"
mkdir -p /tmp/hyperledger/medblocks/ca/crypto/
mkdir -p /tmp/hyperledger/medblocks/tmp/
mkdir -p /tmp/hyperledger/tls-ca/crypto/
cp /tmp/hyperledger/tls/ca/crypto/tls-cert.pem /tmp/hyperledger/tls-ca/crypto/tls-ca-cert.pem
echo "------------------------------ Setting up Medblocks CA ------------------------------"
docker-compose -f docker/Medblocks/medblocks-ca-docker-compose.yaml up -d
sleep 1
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/medblocks/ca/crypto/ca-cert.pem
export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/medblocks/ca/admin
fabric-ca-client enroll -d -u https://ca-medblocks-admin:ca-medblocks-adminpw@0.0.0.0:7053
fabric-ca-client affiliation add medblocks -u https://0.0.0.0:7053
fabric-ca-client register -d --id.name orderer-medblocks    --id.affiliation medblocks  --id.secret ordererPW           --id.type orderer   -u https://0.0.0.0:7053
fabric-ca-client register -d --id.name admin      --id.affiliation medblocks  --id.secret medblocksAdminPW    --id.type admin     -u https://0.0.0.0:7053
fabric-ca-client register -d --id.name user1      --id.affiliation medblocks  --id.secret medblocksUserPW    --id.type user     -u https://0.0.0.0:7053
fabric-ca-client register -d --id.name peer1-medblocks      --id.affiliation medblocks  --id.secret peer1PW             --id.type peer      -u https://0.0.0.0:7053


echo "--------------------------------- Setting up sana CA --------------------------------"
# docker-compose -f docker/Sana/sana-ca-docker-compose.yaml up -d
# sleep 1
# export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/sana/ca/crypto/ca-cert.pem
# export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/sana/ca/admin
# fabric-ca-client enroll -d -u https://ca-sana-admin:ca-sana-adminpw@0.0.0.0:7055
# fabric-ca-client affiliation add sana -u https://0.0.0.0:7055
# fabric-ca-client register -d --id.name peer1-sana   --id.affiliation sana --id.secret peer1PW         --id.type peer  -u https://0.0.0.0:7055  --id.attrs '"hf.Registrar.Roles=peer,oracle,client"'
# fabric-ca-client register -d --id.name peer2-sana   --id.affiliation sana --id.secret peer2PW         --id.type peer  -u https://0.0.0.0:7055  --id.attrs '"hf.Registrar.Roles=peer,client"'
# fabric-ca-client register -d --id.name admin-sana   --id.affiliation sana --id.secret sanaAdminPW     --id.type admin -u https://0.0.0.0:7055  --id.attrs '"hf.Registrar.Roles=peer,oracle,client"'
# fabric-ca-client register -d --id.name user-sana    --id.affiliation sana --id.secret sanaUserPW      --id.type user  -u https://0.0.0.0:7055


echo "----------------------------------- COPYING FILES -----------------------------------"
mkdir -p /tmp/hyperledger/medblocks/orderer/assets/ca/
mkdir -p /tmp/hyperledger/medblocks/orderer/assets/tls-ca/
cp /tmp/hyperledger/medblocks/ca/crypto/ca-cert.pem /tmp/hyperledger/medblocks/orderer/assets/ca/medblocks-ca-cert.pem
cp /tmp/hyperledger/tls/ca/crypto/tls-cert.pem      /tmp/hyperledger/medblocks/orderer/assets/tls-ca/tls-ca-cert.pem
#
mkdir -p /tmp/hyperledger/medblocks/peer1/assets/ca/
mkdir -p /tmp/hyperledger/medblocks/peer1/assets/tls-ca/
cp /tmp/hyperledger/medblocks/ca/crypto/ca-cert.pem  /tmp/hyperledger/medblocks/peer1/assets/ca/medblocks-ca-cert.pem
cp /tmp/hyperledger/tls/ca/crypto/tls-cert.pem      /tmp/hyperledger/medblocks/peer1/assets/tls-ca/tls-ca-cert.pem
#
# mkdir -p /tmp/hyperledger/sana/peer1/assets/ca/
# mkdir -p /tmp/hyperledger/sana/peer1/assets/tls-ca/
# mkdir -p /tmp/hyperledger/sana/peer2/assets/ca/
# mkdir -p /tmp/hyperledger/sana/peer2/assets/tls-ca/
# cp /tmp/hyperledger/sana/ca/crypto/ca-cert.pem /tmp/hyperledger/sana/peer1/assets/ca/sana-ca-cert.pem
# cp /tmp/hyperledger/sana/ca/crypto/ca-cert.pem /tmp/hyperledger/sana/peer2/assets/ca/sana-ca-cert.pem
# cp /tmp/hyperledger/tls/ca/crypto/tls-cert.pem /tmp/hyperledger/sana/peer1/assets/tls-ca/tls-ca-cert.pem
# cp /tmp/hyperledger/tls/ca/crypto/tls-cert.pem /tmp/hyperledger/sana/peer2/assets/tls-ca/tls-ca-cert.pem



# echo "---------------------------- Registering Sana identities ----------------------------"
# export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/sana/peer1
# export FABRIC_CA_CLIENT_MSPDIR=msp
# export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/sana/peer1/assets/ca/sana-ca-cert.pem
# fabric-ca-client enroll -d -u https://peer1-sana:peer1PW@0.0.0.0:7055

# export FABRIC_CA_CLIENT_MSPDIR=tls-msp
# export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/sana/peer1/assets/tls-ca/tls-ca-cert.pem
# fabric-ca-client enroll -d -u https://peer1-sana:peer1PW@0.0.0.0:7052 --enrollment.profile tls --csr.hosts peer1-sana

# mv /tmp/hyperledger/sana/peer1/tls-msp/keystore/* /tmp/hyperledger/sana/peer1/tls-msp/keystore/key.pem


# export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/sana/peer2
# export FABRIC_CA_CLIENT_MSPDIR=msp
# export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/sana/peer2/assets/ca/sana-ca-cert.pem
# fabric-ca-client enroll -d -u https://peer2-sana:peer2PW@0.0.0.0:7055

# export FABRIC_CA_CLIENT_MSPDIR=tls-msp
# export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/sana/peer2/assets/tls-ca/tls-ca-cert.pem
# fabric-ca-client enroll -d -u https://peer2-sana:peer2PW@0.0.0.0:7052 --enrollment.profile tls --csr.hosts peer2-sana

# mv /tmp/hyperledger/sana/peer2/tls-msp/keystore/* /tmp/hyperledger/sana/peer2/tls-msp/keystore/key.pem

# export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/sana/admin
# export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/sana/peer1/assets/ca/sana-ca-cert.pem
# export FABRIC_CA_CLIENT_MSPDIR=msp
# fabric-ca-client enroll -d -u https://admin-sana:sanaAdminPW@0.0.0.0:7055

# mkdir -p /tmp/hyperledger/sana/peer1/msp/admincerts/
# mkdir -p /tmp/hyperledger/sana/peer2/msp/admincerts/
# cp /tmp/hyperledger/sana/admin/msp/signcerts/cert.pem /tmp/hyperledger/sana/peer1/msp/admincerts/sana-admin-cert.pem
# cp /tmp/hyperledger/sana/admin/msp/signcerts/cert.pem /tmp/hyperledger/sana/peer2/msp/admincerts/sana-admin-cert.pem



echo "-------------------------- Registering Medblocks identities -------------------------"

export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/medblocks/orderer
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/medblocks/orderer/assets/ca/medblocks-ca-cert.pem
fabric-ca-client enroll -d -u https://orderer-medblocks:ordererPW@0.0.0.0:7053

export FABRIC_CA_CLIENT_MSPDIR=tls-msp
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/medblocks/orderer/assets/tls-ca/tls-ca-cert.pem
fabric-ca-client enroll -d -u https://orderer-medblocks:ordererPW@0.0.0.0:7052 --enrollment.profile tls --csr.hosts orderer-medblocks

mv /tmp/hyperledger/medblocks/orderer/tls-msp/keystore/* /tmp/hyperledger/medblocks/orderer/tls-msp/keystore/key.pem

export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/medblocks/peer1
export FABRIC_CA_CLIENT_MSPDIR=msp
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/medblocks/peer1/assets/ca/medblocks-ca-cert.pem
fabric-ca-client enroll -d -u https://peer1-medblocks:peer1PW@0.0.0.0:7053

export FABRIC_CA_CLIENT_MSPDIR=tls-msp
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/medblocks/peer1/assets/tls-ca/tls-ca-cert.pem
fabric-ca-client enroll -d -u https://peer1-medblocks:peer1PW@0.0.0.0:7052 --enrollment.profile tls --csr.hosts peer1-medblocks

mv /tmp/hyperledger/medblocks/peer1/tls-msp/keystore/* /tmp/hyperledger/medblocks/peer1/tls-msp/keystore/key.pem

export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/medblocks/users/admin
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/medblocks/orderer/assets/ca/medblocks-ca-cert.pem
export FABRIC_CA_CLIENT_MSPDIR=msp
fabric-ca-client enroll -d -u https://admin:medblocksAdminPW@0.0.0.0:7053
cp /tmp/hyperledger/medblocks/users/admin/msp/signcerts/cert.pem /tmp/hyperledger/medblocks/users/admin/msp/signcerts/admin@medblocks-cert.pem 


export FABRIC_CA_CLIENT_HOME=/tmp/hyperledger/medblocks/users/user1
export FABRIC_CA_CLIENT_TLS_CERTFILES=/tmp/hyperledger/medblocks/orderer/assets/ca/medblocks-ca-cert.pem
export FABRIC_CA_CLIENT_MSPDIR=msp
fabric-ca-client enroll -d -u https://user1:medblocksUserPW@0.0.0.0:7053
cp /tmp/hyperledger/medblocks/users/user1/msp/signcerts/cert.pem /tmp/hyperledger/medblocks/users/user1/msp/signcerts/user1@medblocks-cert.pem 

mkdir -p /tmp/hyperledger/medblocks/orderer/msp/admincerts/
mkdir -p /tmp/hyperledger/medblocks/peer1/msp/admincerts/
mkdir -p /tmp/hyperledger/medblocks/user1/msp/admincerts/
cp /tmp/hyperledger/medblocks/users/admin/msp/signcerts/admin@medblocks-cert.pem       /tmp/hyperledger/medblocks/orderer/msp/admincerts/medblocks-admin-cert.pem
cp /tmp/hyperledger/medblocks/users/admin/msp/signcerts/admin@medblocks-cert.pem       /tmp/hyperledger/medblocks/peer1/msp/admincerts/medblocks-admin-cert.pem
cp /tmp/hyperledger/medblocks/users/admin/msp/signcerts/admin@medblocks-cert.pem       /tmp/hyperledger/medblocks/user1/msp/admincerts/medblocks-admin-cert.pem



echo "-------------------------- Registering Medblocks identities --------------------------"
echo "-------------------------------- Copying Files --------------------------------------"
mkdir -p /tmp/hyperledger/medblocks/msp/admincerts/
mkdir -p /tmp/hyperledger/medblocks/msp/cacerts/
mkdir -p /tmp/hyperledger/medblocks/msp/tlscacerts/
mkdir -p /tmp/hyperledger/medblocks/msp/users/
mkdir -p /tmp/hyperledger/medblocks/users/admin/msp/admincerts/
cp /tmp/hyperledger/medblocks/users/admin/msp/signcerts/admin@medblocks-cert.pem           /tmp/hyperledger/medblocks/msp/admincerts/admin-medblocks-cert.pem
cp /tmp/hyperledger/medblocks/users/admin/msp/signcerts/admin@medblocks-cert.pem           /tmp/hyperledger/medblocks/users/admin/msp/admincerts/admin-medblocks-cert.pem
cp /tmp/hyperledger/tls/ca/crypto/ca-cert.pem                       /tmp/hyperledger/medblocks/msp/tlscacerts/tls-ca-cert.pem
cp /tmp/hyperledger/medblocks/users/admin/msp/cacerts/0-0-0-0-7053.pem    /tmp/hyperledger/medblocks/msp/cacerts/medblocks-ca-cert.pem

# mkdir -p /tmp/hyperledger/sana/msp/admincerts/
# mkdir -p /tmp/hyperledger/sana/msp/cacerts/
# mkdir -p /tmp/hyperledger/sana/msp/tlscacerts/
# mkdir -p /tmp/hyperledger/sana/msp/users/
# mkdir -p /tmp/hyperledger/sana/admin/msp/admincerts/
# cp /tmp/hyperledger/sana/admin/msp/signcerts/cert.pem           /tmp/hyperledger/sana/msp/admincerts/admin-sana-cert.pem
# cp /tmp/hyperledger/sana/admin/msp/signcerts/cert.pem           /tmp/hyperledger/sana/admin/msp/admincerts/admin-sana-cert.pem
# cp /tmp/hyperledger/tls/ca/crypto/ca-cert.pem                   /tmp/hyperledger/sana/msp/tlscacerts/tls-ca-cert.pem
# cp /tmp/hyperledger/sana/admin/msp/cacerts/0-0-0-0-7055.pem     /tmp/hyperledger/sana/msp/cacerts/sana-ca-cert.pem

echo "---------------------------- GENERATE CHANNEL ARTIFACTS -----------------------------"
cd config
sudo ../bin/configtxgen -profile OrgsOrdererGenesis -outputBlock /tmp/hyperledger/medblocks/orderer/genesis.block
sudo ../bin/configtxgen -profile OrgsChannel        -outputCreateChannelTx /tmp/hyperledger/medblocks/orderer/channel.tx -channelID mychannel
cd -

cp /tmp/hyperledger/medblocks/orderer/channel.tx /tmp/hyperledger/medblocks/peer1/assets/channel.tx

chmod 777 /tmp/hyperledger -R

docker-compose -f docker/Medblocks/orderer-docker-compose.yaml  up -d
docker-compose -f docker/Medblocks/peers-docker-compose.yaml     up -d
# docker-compose -f docker/Sana/peers-docker-compose.yaml         up -d
docker-compose -f docker/Medblocks/cli-docker-compose.yaml       up -d
# docker-compose -f docker/Sana/cli-docker-compose.yaml           up -d

sleep 5
echo "--------------------------- CREATE CHANNEL ---------------------------------------"
docker exec -it cli-medblocks bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/users/admin/msp peer channel create -c mychannel -f /tmp/hyperledger/medblocks/peer1/assets/channel.tx -o orderer-medblocks:7050 --outputBlock /tmp/hyperledger/medblocks/peer1/assets/mychannel.block --tls --cafile /tmp/hyperledger/medblocks/peer1/tls-msp/tlscacerts/tls-0-0-0-0-7052.pem"
# cp /tmp/hyperledger/medblocks/peer1/assets/mychannel.block /tmp/hyperledger/sana/peer1/assets/mychannel.block
# cp /tmp/hyperledger/medblocks/peer1/assets/mychannel.block /tmp/hyperledger/sana/peer2/assets/mychannel.block
cp /tmp/hyperledger/medblocks/peer1/assets/mychannel.block /tmp/hyperledger/medblocks/peer1/assets/mychannel.block
# echo "----------------------------- CHANNEL JOIN SANA P1 -------------------------------"
# docker exec -it cli-sana     bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/sana/admin/msp       CORE_PEER_ADDRESS=peer1-sana:7051       peer channel join -b /tmp/hyperledger/sana/peer1/assets/mychannel.block"
# echo "----------------------------- CHANNEL JOIN SANA P2 -------------------------------"
# docker exec -it cli-sana     bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/sana/admin/msp       CORE_PEER_ADDRESS=peer2-sana:7051       peer channel join -b /tmp/hyperledger/sana/peer2/assets/mychannel.block"
echo "----------------------------- CHANNEL JOIN MEDB P1 -------------------------------"
sleep 2
docker exec -it cli-medblocks bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/users/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer channel join -b /tmp/hyperledger/medblocks/peer1/assets/mychannel.block"

# echo "----------------------------- CHAINCODE SANA P1 -------------------------------"
# docker exec -it cli-sana     bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/sana/admin/msp       CORE_PEER_ADDRESS=peer1-sana:7051       peer chaincode install -n userF -v 8.0 -p gitlab.com/medblocks/userFunctions"
# docker exec -it cli-sana     bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/sana/admin/msp       CORE_PEER_ADDRESS=peer1-sana:7051       peer chaincode install -n fileF -v 8.0 -p gitlab.com/medblocks/FileAssetHandlers"
# echo "----------------------------- CHAINCODE SANA P2 -------------------------------"
# docker exec -it cli-sana     bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/sana/admin/msp       CORE_PEER_ADDRESS=peer2-sana:7051       peer chaincode install -n userF -v 8.0 -p gitlab.com/medblocks/userFunctions"
# docker exec -it cli-sana     bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/sana/admin/msp       CORE_PEER_ADDRESS=peer2-sana:7051       peer chaincode install -n fileF -v 8.0 -p gitlab.com/medblocks/FileAssetHandlers"
echo "----------------------------- CHAINCODE MEDB P1 -------------------------------"
docker exec -it cli-medblocks bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/users/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer chaincode install -n userF -v 1.100 -p gitlab.com/medblocks/hyperledgerchaincode/mains/usermain"
docker exec -it cli-medblocks bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/users/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer chaincode install -n fileF -v 1.100 -p gitlab.com/medblocks/hyperledgerchaincode/mains/filemain"
docker exec -it cli-medblocks bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/users/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer chaincode install -n asset -v 1.100 -p gitlab.com/medblocks/hyperledgerchaincode/mains/assetmain"
echo "----------------------------- CHAINCODE INSTANTIATE userF-------------------------------"
docker exec -it cli-medblocks bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/users/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer chaincode instantiate -C mychannel -n userF -v 1.100 -c '{\"Args\":[\"init\", \"AA\", \"BB\"]}' -o orderer-medblocks:7050 --tls --cafile /tmp/hyperledger/medblocks/peer1/tls-msp/tlscacerts/tls-0-0-0-0-7052.pem"
echo "----------------------------- CHAINCODE INSTANTIATE fileF-------------------------------"
docker exec -it cli-medblocks bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/users/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer chaincode instantiate -C mychannel -n fileF -v 1.100 -c '{\"Args\":[\"init\"]}' -o orderer-medblocks:7050 --tls --cafile /tmp/hyperledger/medblocks/peer1/tls-msp/tlscacerts/tls-0-0-0-0-7052.pem"
echo "----------------------------- CHAINCODE INSTANTIATE asset-------------------------------"
docker exec -it cli-medblocks bash -c "CORE_PEER_MSPCONFIGPATH=/tmp/hyperledger/medblocks/users/admin/msp   CORE_PEER_ADDRESS=peer1-medblocks:7051   peer chaincode instantiate -C mychannel -n asset -v 1.100 -c '{\"Args\":[\"init\"]}' -o orderer-medblocks:7050 --tls --cafile /tmp/hyperledger/medblocks/peer1/tls-msp/tlscacerts/tls-0-0-0-0-7052.pem"

