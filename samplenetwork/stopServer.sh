export IMAGE_TAG=latest
#removing containers                                                              

echo "Stopping cli ..."
docker-compose -f docker/Medblocks/cli-docker-compose.yaml stop
docker-compose -f docker/Sana/cli-docker-compose.yaml stop

echo "Stopping orderer..."
docker-compose -f docker/Medblocks/orderer-docker-compose.yaml stop

echo "Stopping peers..."
docker-compose -f docker/Medblocks/peers-docker-compose.yaml stop
docker-compose -f docker/Sana/peers-docker-compose.yaml stop

echo "Stopping CA servers..."
docker-compose -f docker/Medblocks/medblocks-ca-docker-compose.yaml stop
docker-compose -f docker/Indicare/indicare-ca-docker-compose.yaml stop
docker-compose -f docker/Sana/sana-ca-docker-compose.yaml stop
docker-compose -f docker/ca-tls/ca-tls-docker-compose.yaml stop
