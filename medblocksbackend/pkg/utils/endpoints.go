package utils

import "fmt"

var DatabaseEndpoint = "127.0.0.1"
var ConfigLocation = "./config/backendConfig.json"
var HyperledgerConfigLocation = "./config/hyperledgerConfigLocal.yaml"

func SetEndpoints(docker *bool) {
	fmt.Println("here1")
	if *docker {
		fmt.Println("here")
		DatabaseEndpoint = "couchdb"
		ConfigLocation = "/go/bin/config/backendConfig.json"
		HyperledgerConfigLocation = "/go/bin/config/hyperledgerConfigDocker.yaml"
	}
}
