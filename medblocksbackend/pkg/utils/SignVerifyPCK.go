package utils

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/pem"
)

func VerifySignature(message string, signature []byte, key *rsa.PublicKey) bool {
	hasher := sha256.New()
	hasher.Write([]byte(message))
	hashed := hasher.Sum(nil)
	err := rsa.VerifyPKCS1v15(
		key,
		crypto.SHA256,
		hashed,
		signature,
	)
	if err != nil {
		return false
	} else {
		return true
	}
}

func VerifyPublicKey(key string) bool {
	block, _ := pem.Decode([]byte(key))
	if block == nil {
		return false
	}
	_, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return false
	}
	return true
}
