package hyperledgerinterface

import (
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
)

// QueryHello query the chaincode to get the state of hello
func (setup *ChaincodeSetup) Execute(functionName string, args []string) (string, error) {
	var argBytes [][]byte
	for _, v := range args {
		argBytes = append(argBytes, []byte(v))
	}
	response, err := setup.ChannelClient.Execute(channel.Request{ChaincodeID: setup.ChainCodeID, Fcn: functionName, Args: argBytes})
	if err != nil {
		return "", fmt.Errorf("failed to query: %v", err)
	}

	return string(response.Payload), nil
}
func (setup *ChaincodeSetup) Query(functionName string, args []string) (string, error) {
	var argBytes [][]byte
	for _, v := range args {
		argBytes = append(argBytes, []byte(v))
	}
	response, err := setup.ChannelClient.Query(channel.Request{ChaincodeID: setup.ChainCodeID, Fcn: functionName, Args: argBytes})
	if err != nil {
		return "", fmt.Errorf("failed to query: %v", err)
	}

	return string(response.Payload), nil
}
