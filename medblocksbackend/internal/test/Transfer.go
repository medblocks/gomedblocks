package test

import (
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
)

func Transfer(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	return execCC(ctx.AssetStub, "transfer", signedRequestObject.Data, signedRequestObject.Signature)
}
