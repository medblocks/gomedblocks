package test

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/database"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/users"
)

func StoreKey(userObject users.UserStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	newReq := users.UserStruct{
		EmailID:     userObject.EmailID,
		EPrivateKey: userObject.EPrivateKey,
		SPrivateKey: userObject.SPrivateKey,
		IV:          users.IVPairStruct{IVE: userObject.IV.IVE, IVS: userObject.IV.IVS},
	}
	err = database.TestStoreKeys(ctx.DBClient, newReq)
	if err != nil {
		statusCode = http.StatusInternalServerError
		err = fmt.Errorf("database storekeys error: %e", err)
		return
	}
	tmp, _ := json.Marshal(userObject)
	message = string(tmp)
	statusCode = http.StatusAccepted
	err = nil
	return
}
