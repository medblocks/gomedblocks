package test

import (
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/tokens"
)

func BalanceOfAdmin(tokenObject tokens.TokenStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	return execCC(ctx.AssetStub, "balanceOfAdmin", tokenObject.AssetName)
}
