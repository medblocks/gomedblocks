package test

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/database"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/users"
)

func GetKey(userObject users.UserStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	userExists, userData := database.TestGetKeys(ctx.DBClient, userObject.EmailID)
	if err != nil {
		statusCode = http.StatusInternalServerError
		err = fmt.Errorf("Database GetKey error: %e", err)
		return
	}
	if !userExists {
		statusCode = http.StatusNotFound
		err = fmt.Errorf("Does not Exist: %s", userObject.EmailID)
		return
	}
	fmt.Println("Keys from request: ", userData.EmailID)
	response, _ := json.Marshal(userData)
	message = string(response)
	statusCode = http.StatusAccepted
	err = nil
	return
}
