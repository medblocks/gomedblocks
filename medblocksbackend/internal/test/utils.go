package test

import (
	"fmt"
	"net/http"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func execCC(stub *shim.MockStub, function string, vals ...string) (message string, statusCode int, err error) {
	data := [][]byte{[]byte(function)}
	for _, v := range vals {
		data = append(data, []byte(v))
	}
	resp := stub.MockInvoke("001", data)

	// err = registerUserDb(req)
	if resp.Status != shim.OK {
		statusCode = http.StatusInternalServerError
		err = fmt.Errorf(string(resp.Message))
		return
	}
	message = string(resp.Payload)
	statusCode = http.StatusAccepted
	err = nil
	return
}
