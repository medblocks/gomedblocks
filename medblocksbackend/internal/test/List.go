package test

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
)

type permission struct {
	ReceiverEmailID string `json:"receiverEmailId"`
	ReceiverKey     string `json:"receiverKey"`
}
type fileListRecord struct {
	IpfsHash     string       `json:"ipfsHash"`
	Name         string       `json:"name"`
	Format       string       `json:"format"`
	OwnerEmailID string       `json:"ownerEmailId"`
	Permissions  []permission `json:"permissions"`
}

func List(OwnerEmailID string, PermittedEmailID string, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	data := [][]byte{[]byte("getFileList")}
	vals := []string{"", ""}
	for _, v := range vals {
		data = append(data, []byte(v))
	}
	resp := ctx.FileStub.MockInvoke("001", data)

	if resp.Status != shim.OK {
		statusCode = http.StatusInternalServerError
		err = fmt.Errorf(string(resp.Message))
		return
	}
	fmt.Println(resp.Payload)
	type a struct {
		List []fileListRecord `json:"list"`
	}
	var files, files2 a
	e := json.Unmarshal(resp.Payload, &files)
	fmt.Println(e, files)
	for _, file := range files.List {
		pass := true
		if OwnerEmailID != "" {
			if file.OwnerEmailID != OwnerEmailID {
				continue
			}
		}
		if PermittedEmailID != "" {
			pass = false
			for _, perm := range file.Permissions {
				if perm.ReceiverEmailID == PermittedEmailID {
					pass = true
					break
				}
			}
			if !pass {
				continue
			}
		}
		files2.List = append(files2.List, file)
	}
	response, _ := json.Marshal(files2)

	message = string(response)
	err = nil
	statusCode = http.StatusAccepted
	return
}
