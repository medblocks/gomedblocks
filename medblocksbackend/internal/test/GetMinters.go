package test

import (
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/tokens"
)

func GetMinters(tokenObject tokens.TokenStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	return execCC(ctx.AssetStub, "getMinters", tokenObject.AssetName)
}
