package backendcontext

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"gitlab.com/medblocks/hyperledgerchaincode/assetfunctions"
	"gitlab.com/medblocks/hyperledgerchaincode/fileassethandlers"
	"gitlab.com/medblocks/hyperledgerchaincode/userfunctions"
)

func (ctx *BackendContextStruct) SetupStubs() {
	ctx.UserStub = shim.NewMockStub("userF", new(userfunctions.RegisterUserSmartContract))
	ctx.FileStub = shim.NewMockStub("fileF", new(fileassethandlers.FileAssetHandlers))
	ctx.AssetStub = shim.NewMockStub("assets", new(assetfunctions.Asset))
	ctx.UserStub.MockInit("1", [][]byte{})
	ctx.FileStub.MockInit("1", [][]byte{})
	ctx.AssetStub.MockInit("1", [][]byte{})
	ctx.UserStub.MockPeerChaincode("fileF", ctx.FileStub)
	ctx.UserStub.MockPeerChaincode("assetF", ctx.AssetStub)
	ctx.FileStub.MockPeerChaincode("userF", ctx.UserStub)
	ctx.FileStub.MockPeerChaincode("assetF", ctx.AssetStub)
	ctx.AssetStub.MockPeerChaincode("fileF", ctx.FileStub)
	ctx.AssetStub.MockPeerChaincode("userF", ctx.UserStub)

}
