package backendcontext

import (
	"github.com/fjl/go-couchdb"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

type BackendContextStruct struct {
	DBClient    *couchdb.Client
	ClientSetup hi.FabricSetup
	Test        bool
	UserStub    *shim.MockStub
	FileStub    *shim.MockStub
	AssetStub   *shim.MockStub
}
