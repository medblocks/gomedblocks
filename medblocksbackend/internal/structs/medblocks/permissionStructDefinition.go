package medblocks

type PermissionStruct struct {
	IPFSHash        string `json:"ipfsHash,omitempty"`
	ReceiverEmailID string `json:"receiverEmailId"`
	ReceiverKey     string `json:"receiverKey"`
}
