package medblocks

type MedblockStruct struct {
	IPFSHash         string       `json:"ipfsHash,omitempty"`
	IV               string       `json:"IV"`
	Name             string       `json:"name,omitempty"`
	Format           string       `json:"format,omitempty"`
	SignatoryEmailID string       `json:"signatoryEmailId,omitempty"`
	OwnerEmailID     string       `json:"ownerEmailId,omitempty"`
	SenderEmailID    string       `json:"senderEmailId,omitempty"`
	PermittedEmailID string       `json:"permittedEmailId,omitempty"`
	Permissions      []PermissionStruct `json:"permissions,omitempty"`
}
