package tokens
type TokenStruct struct {
	AssetName string `json:"assetName"`
	Target    string `json:"target,omitempty"`
}