package users

type IVPairStruct struct {
	IVS string `json:"ivs`
	IVE string `json:"ive`
}
type UserStruct struct {
	EmailID          string       `json:"emailId"`
	IV               IVPairStruct `json:"IV"`
	SPublicKey       string       `json:"sPublicKey,omitempty"`
	EPublicKey       string       `json:"ePublicKey,omitempty"`
	SPrivateKey      string       `json:"sPrivateKey,omitempty"`
	EPrivateKey      string       `json:"ePrivateKey,omitempty"`
	Metadata         string       `json:"metadata,omitempty"`
	IdentityFileHash string       `json:"identityFileHash,omitempty"`
}
