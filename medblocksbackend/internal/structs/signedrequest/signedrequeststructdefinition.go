package signedrequest

type SignedRequestsStruct struct {
	Data      string `json:data`
	Signature string `json:signature`
}