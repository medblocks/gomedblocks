package database

import (
	"github.com/fjl/go-couchdb"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/users"
)

//StoreKeys stores keys in live database
func StoreKeys(cl *couchdb.Client, doc users.UserStruct) error {
	var db *couchdb.DB
	var err error
	db, err = cl.EnsureDB("key")
	if err != nil {
		return err
	}
	_, err = db.Put(doc.EmailID, doc, "")
	return err
}

//GetKeys gets keys from live database
func GetKeys(cl *couchdb.Client, EmailID string) (exists bool, user users.UserStruct) {
	db, err := cl.EnsureDB("key")
	if err != nil {
		return false, user
	}
	err = db.Get(EmailID, &user, nil)
	if err != nil {
		return false, user
	}
	return true, user
}

//TestStoreKeys stores keys in test database
func TestStoreKeys(cl *couchdb.Client, doc users.UserStruct) error {
	var db *couchdb.DB
	var err error
	db, err = cl.EnsureDB("testkey")
	if err != nil {
		return err
	}
	_, err = db.Put(doc.EmailID, doc, "")
	return err
}

//TestGetKeys gets keys from test database
func TestGetKeys(cl *couchdb.Client, EmailID string) (exists bool, user users.UserStruct) {
	db, err := cl.EnsureDB("testkey")
	if err != nil {
		return false, user
	}
	err = db.Get(EmailID, &user, nil)
	if err != nil {
		return false, user
	}
	return true, user
}
