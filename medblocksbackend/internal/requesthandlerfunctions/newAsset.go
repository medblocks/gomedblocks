package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func NewAssetHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var newAssetFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		newAssetFunc = test.NewAsset
	} else {
		newAssetFunc = newAsset
	}
	return func(c *gin.Context) {
		EnableCors(c)
		signedRequestObject, statusCode, err := newAssetRequestInit(c)
		newAssetLog := fmt.Sprintln("NewAsset request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, newAssetLog)
			return
		}
		message, statusCode, err := newAssetFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, newAssetLog)
			return
		}
		RenderSuccess(c, message, statusCode, newAssetLog)
	}
}
func newAssetRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	signedRequestObject, statusCode, err = signedRequestInit(c)
	if err != nil {
		return
	}
	type Params struct {
		AssetName      string   `json:"assetName"`
		AssetAdmin     string   `json:"assetAdmin"`
		AssetMinters   []string `json:"assetMinters"`
		InitialBalance int      `json:"balance"`
	}
	var params Params
	json.Unmarshal([]byte(signedRequestObject.Data), &params)
	if params.AssetName == "" || params.AssetAdmin == "" || params.AssetMinters == nil {
		missing := "Missing Required fields:"
		if params.AssetName == "" {
			missing += " assetName"
		}
		if params.AssetAdmin == "" {
			missing += " assetAdmin"
		}
		if params.AssetMinters == nil {
			missing += " assetMinters"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}
func newAsset(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("newAsset", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
