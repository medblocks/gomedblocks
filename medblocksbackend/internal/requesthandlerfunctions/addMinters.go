package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

//AddMintersHandlerFactory creates and returns function to handle AddMinters operation. AddMinters operation adds minters to an asset
func AddMintersHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var addMintersFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		addMintersFunc = test.AddMinters
	} else {
		addMintersFunc = addMinters
	}

	//Return function that handles addMinters call
	return func(c *gin.Context) {
		EnableCors(c)
		signedRequestObject, statusCode, err := addMintersRequestInit(c)
		addMintersLog := fmt.Sprintln("AddMinters request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, addMintersLog)
			return
		}

		//call addMinters
		message, statusCode, err := addMintersFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, addMintersLog)
			return
		}
		RenderSuccess(c, message, statusCode, addMintersLog)
	}
}

//addMintersRequestInit Parses and validates addMinters Request
func addMintersRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	signedRequestObject, statusCode, err = signedRequestInit(c)
	if err != nil {
		return
	}
	type Params struct {
		AssetName string   `json:"assetName"`
		Minters   []string `json:"minters"`
	}
	var params Params
	json.Unmarshal([]byte(signedRequestObject.Data), &params)
	if params.AssetName == "" || params.Minters == nil {
		missing := "Missing Required fields:"
		if params.AssetName == "" {
			missing += " assetName"
		}
		if params.Minters == nil {
			missing += " minters"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}

//addMinters sends addMinters transaction to hyperledger chaincode
func addMinters(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("addMinters", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
