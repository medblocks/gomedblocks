package requesthandlerfunctions

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/users"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func GetUserHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var getUserFunc func(userObject users.UserStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		getUserFunc = test.GetUser
	} else {
		getUserFunc = getUser
	}
	return func(c *gin.Context) {
		EnableCors(c)
		userObject, statusCode, err := getUserRequestInit(c)
		getUserLog := fmt.Sprintln("GetUser request: ", userObject.EmailID)
		if err != nil {
			RenderError(c, err, statusCode, getUserLog)
			return
		}
		message, statusCode, err := getUserFunc(userObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, getUserLog)
			return
		}
		RenderSuccess(c, message, statusCode, getUserLog)
	}
}

func getUser(userObject users.UserStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "userF",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Query("queryUser", []string{userObject.EmailID})
	if err != nil {
		err = fmt.Errorf("Query Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	err = nil
	statusCode = http.StatusFound
	message = string(resp)
	return
}
func getUserRequestInit(c *gin.Context) (userObject users.UserStruct, statusCode int, err error) {
	userObject.EmailID = c.Param("ID")
	fmt.Println("Processing registration request...", userObject.EmailID)
	if userObject.EmailID == "" {
		missing := "Missing Required fields:"
		if userObject.EmailID == "" {
			missing += " emailId"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = 0
	return
}
