package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func TransferToAdminHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var transferToAdminFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		transferToAdminFunc = test.TransferToAdmin
	} else {
		transferToAdminFunc = transferToAdmin
	}
	return func(c *gin.Context) {
		EnableCors(c)

		signedRequestObject, statusCode, err := transferToAdminRequestInit(c)
		transferToAdminLog := fmt.Sprintln("TransferToAdmin request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, transferToAdminLog)
			return
		}
		message, statusCode, err := transferToAdminFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, transferToAdminLog)
			return
		}
		RenderSuccess(c, message, statusCode, transferToAdminLog)
	}
}
func transferToAdminRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	signedRequestObject, statusCode, err = signedRequestInit(c)
	if err != nil {
		return
	}
	type Params struct {
		AssetName string `json:"assetName"`
		From      string `json:"from"`
		Amount    int    `json:"amount"`
	}
	var params Params
	json.Unmarshal([]byte(signedRequestObject.Data), &params)
	if params.AssetName == "" || params.From == "" || params.Amount <= 0 {
		missing := "Missing Required fields:"
		if params.AssetName == "" {
			missing += " assetName"
		}
		if params.From == "" {
			missing += " from"
		}
		if params.Amount <= 0 {
			missing += " amount"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}
func transferToAdmin(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("transferToAdmin", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
