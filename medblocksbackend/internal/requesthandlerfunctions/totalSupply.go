package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/tokens"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func TotalSupplyHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var totalSupplyFunc func(tokenObject tokens.TokenStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		totalSupplyFunc = test.TotalSupply
	} else {
		totalSupplyFunc = totalSupply
	}
	return func(c *gin.Context) {
		EnableCors(c)

		tokenObject, statusCode, err := totalSupplyRequestInit(c)
		totalSupplyLog := fmt.Sprintln("TotalSupply request: ", tokenObject.AssetName)
		if err != nil {
			RenderError(c, err, statusCode, totalSupplyLog)
			return
		}
		message, statusCode, err := totalSupplyFunc(tokenObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, totalSupplyLog)
			return
		}
		RenderSuccess(c, message, statusCode, totalSupplyLog)
	}
}
func totalSupplyRequestInit(c *gin.Context) (tokenObject tokens.TokenStruct, statusCode int, err error) {
	decoder := json.NewDecoder(c.Request.Body)
	err = decoder.Decode(&tokenObject)
	if err != nil {
		statusCode = http.StatusBadRequest
		err = fmt.Errorf("JSON user decode error: %e", err)
		return
	}
	if tokenObject.AssetName == "" {
		missing := "Missing Required fields:"
		if tokenObject.AssetName == "" {
			missing += " assetName"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}
func totalSupply(tokenObject tokens.TokenStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Query("totalSupply", []string{tokenObject.AssetName})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
