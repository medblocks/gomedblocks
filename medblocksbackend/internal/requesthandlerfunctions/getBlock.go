package requesthandlerfunctions

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/medblocks"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func GetBlockHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var getBlockFunc func(medblockObject medblocks.MedblockStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		getBlockFunc = test.GetBlock
	} else {
		getBlockFunc = getBlock
	}
	return func(c *gin.Context) {
		EnableCors(c)
		medblockObject, statusCode, err := getBlockRequestInit(c)
		getBlockLog := fmt.Sprintln("GetBlock request: ", medblockObject.IPFSHash)
		if err != nil {
			RenderError(c, err, statusCode, getBlockLog)
			return
		}
		message, statusCode, err := getBlockFunc(medblockObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, getBlockLog)
			return
		}
		RenderSuccess(c, message, statusCode, getBlockLog)
	}
}

func getBlockRequestInit(c *gin.Context) (medblockObject medblocks.MedblockStruct, statusCode int, err error) {
	medblockObject.IPFSHash = c.Param("ipfshash")
	if medblockObject.IPFSHash == "" {
		missing := "Missing Required fields:"
		if medblockObject.IPFSHash == "" {
			missing += " ipfsHash"
		}
		statusCode = http.StatusBadRequest
		err = fmt.Errorf(missing)
		return
	}
	statusCode = 200
	err = nil
	return
}

func getBlock(medblockObject medblocks.MedblockStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		statusCode = http.StatusInternalServerError
		err = fmt.Errorf("Channel client error: %e", err)
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "fileF",
		ChannelClient: channelClient,
	}
	fmt.Println(medblockObject)
	resp, err := ccClient.Query("getFile", []string{medblockObject.IPFSHash})
	if err != nil {
		statusCode = http.StatusInternalServerError
		err = fmt.Errorf("Query error: %e", err)
		return
	}
	message = resp
	statusCode = http.StatusAccepted
	err = nil
	return
}
