package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/fjl/go-couchdb"
	"github.com/gin-gonic/gin"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/database"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/users"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
)

func StoreKeyHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var storeKeyFunc func(userObject users.UserStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	var getKeyFunc func(cl *couchdb.Client, emailID string) (bool, users.UserStruct)
	if ctx.Test {
		storeKeyFunc = test.StoreKey
		getKeyFunc = database.TestGetKeys
	} else {
		storeKeyFunc = storeKey
		getKeyFunc = database.GetKeys
	}
	return func(c *gin.Context) {
		EnableCors(c)
		userObject, statusCode, err := storeKeyRequestInit(c, ctx, getKeyFunc)
		storeKeyLog := fmt.Sprintln("storeKey request: ", userObject.EmailID)
		if err != nil {
			RenderError(c, err, statusCode, storeKeyLog)
			return
		}
		message, statusCode, err := storeKeyFunc(userObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, storeKeyLog)
			return
		}
		RenderSuccess(c, message, statusCode, storeKeyLog)
	}
}
func storeKeyRequestInit(c *gin.Context, ctx *bctx.BackendContextStruct, getKeyFunc func(cl *couchdb.Client, EmailID string) (exists bool, user users.UserStruct)) (userObject users.UserStruct, statusCode int, err error) {
	decoder := json.NewDecoder(c.Request.Body)
	err = decoder.Decode(&userObject)
	if err != nil {
		err = fmt.Errorf("Json user decode error: %e", err)
		statusCode = http.StatusBadRequest
		return
	}
	fmt.Println("Processing storeKey request...", userObject.EmailID)
	if userObject.EmailID == "" || userObject.SPrivateKey == "" || userObject.EPrivateKey == "" || userObject.IV.IVS == "" || userObject.IV.IVE == "" {
		missing := "Missing Required fields:"
		if userObject.EmailID == "" {
			missing += " emailId"
		}
		if userObject.SPrivateKey == "" {
			missing += " sPrivateKey"
		}
		if userObject.EPrivateKey == "" {
			missing += " ePrivateKey"
		}
		if userObject.IV.IVS == "" {
			missing += " IVS"
		}
		if userObject.IV.IVE == "" {
			missing += " IVE"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	userExists, _ := getKeyFunc(ctx.DBClient, userObject.EmailID)
	if userExists {
		err = fmt.Errorf("Already Exists: %s", userObject.EmailID)
		statusCode = http.StatusConflict
		return
	}
	err = nil
	statusCode = 0
	return
}

func storeKey(userObject users.UserStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	newReq := users.UserStruct{
		EmailID:     userObject.EmailID,
		EPrivateKey: userObject.EPrivateKey,
		SPrivateKey: userObject.SPrivateKey,
		IV:          users.IVPairStruct{IVE: userObject.IV.IVE, IVS: userObject.IV.IVS},
	}
	err = database.StoreKeys(ctx.DBClient, newReq)
	if err != nil {
		statusCode = http.StatusInternalServerError
		err = fmt.Errorf("database storekeys error: %e", err)
		return
	}
	tmp, _ := json.Marshal(userObject)
	message = string(tmp)
	statusCode = http.StatusAccepted
	err = nil
	return
}
