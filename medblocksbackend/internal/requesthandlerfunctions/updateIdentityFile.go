package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/users"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func UpdateIdentityFileHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var updateIdentityFileFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		updateIdentityFileFunc = test.UpdateIdentityFile
	} else {
		updateIdentityFileFunc = updateIdentityFile
	}
	return func(c *gin.Context) {
		EnableCors(c)
		signedRequestObject, statusCode, err := updateIdentityFileRequestInit(c)
		updateIdentityFileLog := fmt.Sprintln("UpdateIdentityFile request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, updateIdentityFileLog)
			return
		}
		message, statusCode, err := updateIdentityFileFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, updateIdentityFileLog)
			return
		}
		RenderSuccess(c, message, statusCode, updateIdentityFileLog)
	}
}

func updateIdentityFileRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	signedRequestObject, statusCode, err = signedRequestInit(c)
	if err != nil {
		return
	}
	var userObject users.UserStruct
	json.Unmarshal([]byte(signedRequestObject.Data), &userObject)
	if userObject.EmailID == "" || userObject.IdentityFileHash == "" {
		missing := "Missing Required fields:"
		if userObject.EmailID == "" {
			missing += " emailId"
		}
		if userObject.IdentityFileHash == "" {
			missing += " identityFileHash"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = 200
	return
}

func updateIdentityFile(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "userF",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("updateIdentityFile", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	statusCode = http.StatusAccepted
	err = nil
	return
}
