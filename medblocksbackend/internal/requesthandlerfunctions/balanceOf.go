package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/tokens"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func BalanceOfHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var balanceOfFunc func(tokenObject tokens.TokenStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		balanceOfFunc = test.BalanceOf
	} else {
		balanceOfFunc = balanceOf
	}
	return func(c *gin.Context) {
		EnableCors(c)

		tokenObject, statusCode, err := balanceOfRequestInit(c)
		balanceOfLog := fmt.Sprintln("BalanceOf request: ", tokenObject.AssetName)
		if err != nil {
			RenderError(c, err, statusCode, balanceOfLog)
			return
		}
		message, statusCode, err := balanceOfFunc(tokenObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, balanceOfLog)
			return
		}
		RenderSuccess(c, message, statusCode, balanceOfLog)
	}
}
func balanceOfRequestInit(c *gin.Context) (tokenObject tokens.TokenStruct, statusCode int, err error) {
	decoder := json.NewDecoder(c.Request.Body)
	err = decoder.Decode(&tokenObject)
	if err != nil {
		statusCode = http.StatusBadRequest
		err = fmt.Errorf("JSON user decode error: %e", err)
		return
	}
	if tokenObject.AssetName == "" || tokenObject.Target == "" {
		missing := "Missing Required fields:"
		if tokenObject.AssetName == "" {
			missing += " assetName"
		}
		if tokenObject.Target == "" {
			missing += " target"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}
func balanceOf(tokenObject tokens.TokenStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Query("balanceOf", []string{tokenObject.AssetName})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
