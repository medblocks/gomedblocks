package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func RemoveMintersHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var removeMintersFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		removeMintersFunc = test.RemoveMinters
	} else {
		removeMintersFunc = removeMinters
	}
	return func(c *gin.Context) {
		EnableCors(c)
		signedRequestObject, statusCode, err := removeMintersRequestInit(c)
		removeMintersLog := fmt.Sprintln("RemoveMinters request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, removeMintersLog)
			return
		}
		message, statusCode, err := removeMintersFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, removeMintersLog)
			return
		}
		RenderSuccess(c, message, statusCode, removeMintersLog)
	}
}
func removeMintersRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	signedRequestObject, statusCode, err = signedRequestInit(c)
	if err != nil {
		return
	}
	type Params struct {
		AssetName string   `json:"assetName"`
		Minters   []string `json:"minters"`
	}
	var params Params
	json.Unmarshal([]byte(signedRequestObject.Data), &params)
	if params.AssetName == "" || params.Minters == nil {
		missing := "Missing Required fields:"
		if params.AssetName == "" {
			missing += " assetName"
		}
		if params.Minters == nil {
			missing += " minters"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}
func removeMinters(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("removeMinters", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
