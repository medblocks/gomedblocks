package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
)

func EnableCors(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
}

func RenderError(c *gin.Context, err error, statusCode int, requestLog string) {
	c.JSON(statusCode, gin.H{"error": err.Error()})
	requestLog += fmt.Sprint("Encountered Error: ", err)
	fmt.Println(requestLog)
}
func RenderSuccess(c *gin.Context, message string, statusCode int, requestLog string) {
	c.String(statusCode, message)
	requestLog += fmt.Sprint("Successfully Finished: ", message)
	fmt.Println(requestLog)
}
func signedRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	decoder := json.NewDecoder(c.Request.Body)
	err = decoder.Decode(&signedRequestObject)
	if err != nil {
		statusCode = http.StatusBadRequest
		err = fmt.Errorf("JSON user decode error: %e", err)
		return
	}
	err = nil
	statusCode = 200
	return
}
