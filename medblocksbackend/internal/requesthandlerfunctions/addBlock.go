package requesthandlerfunctions

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

//AddBlockHandlerFactory creates and returns function to handle AddBlock operation. AddBlock operation adds a block to the chain.
func AddBlockHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx

	//Determine function to be called to perform add block
	var addBlockFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		addBlockFunc = test.AddBlock
	} else {
		addBlockFunc = addBlock
	}

	//Return function that handles addBlock call
	return func(c *gin.Context) {
		EnableCors(c)
		signedRequestObject, statusCode, err := signedRequestInit(c)
		addBlockLog := fmt.Sprintln("AddBlock request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, addBlockLog)
			return
		}

		//call addBlock
		message, statusCode, err := addBlockFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, addBlockLog)
			return
		}
		RenderSuccess(c, message, statusCode, addBlockLog)
	}
}

//addBlock sends addBlock transaction to hyperledger chaincode
func addBlock(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "fileF",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("addFile", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Chaincode error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	statusCode = http.StatusAccepted
	err = nil
	return
}
