package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/tokens"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func BalanceOfAdminHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var balanceOfAdminFunc func(tokenObject tokens.TokenStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		balanceOfAdminFunc = test.BalanceOfAdmin
	} else {
		balanceOfAdminFunc = balanceOfAdmin
	}
	return func(c *gin.Context) {
		EnableCors(c)

		tokenObject, statusCode, err := balanceOfAdminRequestInit(c)
		balanceOfAdminLog := fmt.Sprintln("BalanceOfAdmin request: ", tokenObject.AssetName)
		if err != nil {
			RenderError(c, err, statusCode, balanceOfAdminLog)
			return
		}
		message, statusCode, err := balanceOfAdminFunc(tokenObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, balanceOfAdminLog)
			return
		}
		RenderSuccess(c, message, statusCode, balanceOfAdminLog)
	}
}
func balanceOfAdminRequestInit(c *gin.Context) (tokenObject tokens.TokenStruct, statusCode int, err error) {
	decoder := json.NewDecoder(c.Request.Body)
	err = decoder.Decode(&tokenObject)
	if err != nil {
		statusCode = http.StatusBadRequest
		err = fmt.Errorf("JSON user decode error: %e", err)
		return
	}
	if tokenObject.AssetName == "" {
		missing := "Missing Required fields:"
		if tokenObject.AssetName == "" {
			missing += " assetName"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}
func balanceOfAdmin(tokenObject tokens.TokenStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Query("balanceOfAdmin", []string{tokenObject.AssetName})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
