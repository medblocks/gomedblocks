package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/database"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/users"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
)

func GetKeyHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var getKeyFunc func(userObject users.UserStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		getKeyFunc = test.GetKey
	} else {
		getKeyFunc = getKey
	}
	return func(c *gin.Context) {
		EnableCors(c)
		userObject, statusCode, err := getKeyRequestInit(c)
		getKeyLog := fmt.Sprintln("GetKey request: ", userObject.EmailID)
		if err != nil {
			RenderError(c, err, statusCode, getKeyLog)
			return
		}
		message, statusCode, err := getKeyFunc(userObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, getKeyLog)
			return
		}
		RenderSuccess(c, message, statusCode, getKeyLog)
	}
}

func getKey(userObject users.UserStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	userExists, userData := database.GetKeys(ctx.DBClient, userObject.EmailID)
	if err != nil {
		statusCode = http.StatusInternalServerError
		err = fmt.Errorf("Database GetKey error: %e", err)
		return
	}
	if !userExists {
		statusCode = http.StatusNotFound
		err = fmt.Errorf("Does not Exist: %s", userObject.EmailID)
		return
	}
	fmt.Println("Keys from request: ", userData.EmailID)
	response, _ := json.Marshal(userData)
	message = string(response)
	statusCode = http.StatusAccepted
	err = nil
	return
}

func getKeyRequestInit(c *gin.Context) (userObject users.UserStruct, statusCode int, err error) {
	userObject.EmailID = c.Param("ID")
	fmt.Println("Processing registration request...", userObject.EmailID)
	if userObject.EmailID == "" {
		missing := "Missing Required fields:"
		if userObject.EmailID == "" {
			missing += " emailId"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = 202
	return
}
