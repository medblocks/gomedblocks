package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

type listRequest struct {
	OwnerEmailID     string `json:"ownerEmailId"`
	PermittedEmailID string `json:"permittedEmailId"`
}

func ListHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var listFunc func(OwnerEmailID string, PermittedEmailID string, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		listFunc = test.List
	} else {
		listFunc = list
	}
	return func(c *gin.Context) {
		EnableCors(c)
		listRequestObject, statusCode, err := listRequestInit(c)
		listLog := fmt.Sprintln("List request: ", listRequestObject)
		if err != nil {
			RenderError(c, err, statusCode, listLog)
			return
		}
		message, statusCode, err := listFunc(listRequestObject.OwnerEmailID, listRequestObject.PermittedEmailID, ctx)
		if err != nil {
			RenderError(c, err, statusCode, listLog)
			return
		}
		RenderSuccess(c, message, statusCode, listLog)
	}
}
func list(OwnerEmailID string, PermittedEmailID string, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error:  %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "fileF",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Query("getFileList", []string{OwnerEmailID, PermittedEmailID})
	if err != nil {
		err = fmt.Errorf("Execute error:  %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	message = resp
	return
}

func listRequestInit(c *gin.Context) (listRequestObject listRequest, statusCode int, err error) {
	decoder := json.NewDecoder(c.Request.Body)
	err = decoder.Decode(&listRequestObject)
	if err != nil {
		statusCode = http.StatusBadRequest
		err = fmt.Errorf("JSON user decode error: %e", err)
		return
	}
	err = nil
	statusCode = 200
	return
}
