package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func TransferHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var transferFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		transferFunc = test.Transfer
	} else {
		transferFunc = transfer
	}
	return func(c *gin.Context) {
		EnableCors(c)

		signedRequestObject, statusCode, err := transferRequestInit(c)
		transferLog := fmt.Sprintln("Transfer request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, transferLog)
			return
		}
		message, statusCode, err := transferFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, transferLog)
			return
		}
		RenderSuccess(c, message, statusCode, transferLog)
	}
}
func transferRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	signedRequestObject, statusCode, err = signedRequestInit(c)
	if err != nil {
		return
	}
	type Params struct {
		AssetName string `json:"assetName"`
		From      string `json:"from"`
		To        string `json:"to"`
		Amount    int    `json:"amount"`
	}
	var params Params
	json.Unmarshal([]byte(signedRequestObject.Data), &params)
	if params.AssetName == "" || params.From == "" || params.To == "" || params.Amount <= 0 {
		missing := "Missing Required fields:"
		if params.AssetName == "" {
			missing += " assetName"
		}
		if params.From == "" {
			missing += " from"
		}
		if params.To == "" {
			missing += " to"
		}
		if params.Amount <= 0 {
			missing += " amount"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}
func transfer(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("transfer", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
