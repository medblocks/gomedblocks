package requesthandlerfunctions

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func RegisterHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var registerFunc func(signedObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		registerFunc = test.RegisterUser
	} else {
		registerFunc = registerUser
	}
	return func(c *gin.Context) {
		EnableCors(c)
		signedObject, statusCode, err := signedRequestInit(c)
		registerLog := fmt.Sprintln("Registration from request: ", signedObject)
		if err != nil {
			RenderError(c, err, statusCode, registerLog)
			return
		}
		message, statusCode, err := registerFunc(signedObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, registerLog)
			return
		}
		RenderSuccess(c, message, statusCode, registerLog)
	}
}

func registerUser(signedObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "userF",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("registerUser", []string{signedObject.Data, signedObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	return resp, http.StatusAccepted, nil
}
