package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/medblocks"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

//AddPermissionHandlerFactory creates and returns function to handle AddPermission operation. AddPermission operation gives block access to a user
func AddPermissionHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx

	//Determine function to be called to perform add block
	var addPermissionFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		addPermissionFunc = test.AddPermission
	} else {
		addPermissionFunc = addPermission
	}

	//Return function that handles addPermission call
	return func(c *gin.Context) {
		EnableCors(c)
		signedRequestObject, statusCode, err := addPermissionRequestInit(c)
		addPermissionLog := fmt.Sprintln("AddPermission request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, addPermissionLog)
			return
		}

		//call addPermission
		message, statusCode, err := addPermissionFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, addPermissionLog)
			return
		}
		RenderSuccess(c, message, statusCode, addPermissionLog)
	}
}

//addPermissionRequestInit Parses and validates addPermission Request
func addPermissionRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	signedRequestObject, statusCode, err = signedRequestInit(c)
	if err != nil {
		return
	}
	var medblockObject medblocks.MedblockStruct
	json.Unmarshal([]byte(signedRequestObject.Data), &medblockObject)
	if medblockObject.IPFSHash == "" || medblockObject.SenderEmailID == "" || medblockObject.Permissions == nil {
		missing := "Missing Required fields:"
		if medblockObject.IPFSHash == "" {
			missing += " ipfsHash"
		}
		if medblockObject.SenderEmailID == "" {
			missing += " senderEmailId"
		}
		if medblockObject.Permissions == nil {
			missing += " permissions"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = 200
	return
}

//addPermission sends addPermission transaction to hyperledger chaincode
func addPermission(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "fileF",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("authorizeUser", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = http.StatusAccepted
	return
}
