package requesthandlerfunctions

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/signedrequest"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/test"
	hi "gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/hyperledgerinterface"
)

func ChangeAdminHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	var changeAdminFunc func(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error)
	if ctx.Test {
		changeAdminFunc = test.ChangeAdmin
	} else {
		changeAdminFunc = changeAdmin
	}
	return func(c *gin.Context) {
		EnableCors(c)

		signedRequestObject, statusCode, err := changeAdminRequestInit(c)
		changeAdminLog := fmt.Sprintln("ChangeAdmin request: ", signedRequestObject.Data)
		if err != nil {
			RenderError(c, err, statusCode, changeAdminLog)
			return
		}
		message, statusCode, err := changeAdminFunc(signedRequestObject, ctx)
		if err != nil {
			RenderError(c, err, statusCode, changeAdminLog)
			return
		}
		RenderSuccess(c, message, statusCode, changeAdminLog)
	}
}
func changeAdminRequestInit(c *gin.Context) (signedRequestObject signedrequest.SignedRequestsStruct, statusCode int, err error) {
	signedRequestObject, statusCode, err = signedRequestInit(c)
	if err != nil {
		return
	}
	type Params struct {
		AssetName string `json:"assetName"`
		NewAdmin  string `json:"newAdmin"`
	}
	var params Params
	json.Unmarshal([]byte(signedRequestObject.Data), &params)
	if params.AssetName == "" || params.NewAdmin == "" {
		missing := "Missing Required fields:"
		if params.AssetName == "" {
			missing += " assetName"
		}
		if params.NewAdmin == "" {
			missing += " newAdmin"
		}
		err = fmt.Errorf(missing)
		statusCode = http.StatusBadRequest
		return
	}
	err = nil
	statusCode = http.StatusAccepted
	return
}
func changeAdmin(signedRequestObject signedrequest.SignedRequestsStruct, ctx *bctx.BackendContextStruct) (message string, statusCode int, err error) {
	channelClient, err := ctx.ClientSetup.GetChannelClient()
	if err != nil {
		err = fmt.Errorf("Channel client error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	ccClient := hi.ChaincodeSetup{
		ChainCodeID:   "asset",
		ChannelClient: channelClient,
	}
	resp, err := ccClient.Execute("changeAdmin", []string{signedRequestObject.Data, signedRequestObject.Signature})
	if err != nil {
		err = fmt.Errorf("Execute Error: %e", err)
		statusCode = http.StatusInternalServerError
		return
	}
	message = resp
	err = nil
	statusCode = 200
	return
}
