package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"

	"io/ioutil"

	"github.com/fjl/go-couchdb"
	"github.com/gin-gonic/gin"
	rhf "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/requesthandlerfunctions"
	bctx "gitlab.com/medblocks/gomedblocks/medblocksbackend/internal/structs/backendcontext"
	"gitlab.com/medblocks/gomedblocks/medblocksbackend/pkg/utils"
)

//badGatewayHandler handles requests made to gateways that don't amtch any valid pattern
func badGatewayHandler(c *gin.Context) {
	rhf.EnableCors(c)
	c.String(http.StatusBadGateway, "Invalid gateway")
}

//nukeHandlerFactory Creates a nukeHandler Function that nukes the database and recreates mockshims in test mode, gives error in normal mode
func nukeHandlerFactory(_ctx *bctx.BackendContextStruct) func(c *gin.Context) {
	ctx := _ctx
	if ctx.Test {
		return func(c *gin.Context) {
			rhf.EnableCors(c)
			ctx.SetupStubs()

			ctx.DBClient.DeleteDB("testkey")
			c.String(http.StatusOK, "Ok")
		}
	} else {
		return func(c *gin.Context) {
			rhf.EnableCors(c)
			c.JSON(http.StatusMethodNotAllowed, gin.H{"error": "not supported outside testing mode"})
		}
	}
}

func main() {
	var ctx bctx.BackendContextStruct
	var err error

	//parse flags
	testE := flag.Bool("test", false, "pass to run in chaincode testing mode")
	dockerE := flag.Bool("docker", false, "pass twhen running in docker")
	flag.Parse()
	ctx.Test = *testE

	//set endpoints to local services withing docker
	utils.SetEndpoints(dockerE)
	ctx.DBClient, err = couchdb.NewClient("http://"+utils.DatabaseEndpoint+":5984", nil)
	if err != nil {
		fmt.Printf("Unable to initialize the CouchDB Client: %v\n", err)
		return
	}

	//Initialize backend context
	if ctx.Test {
		//setup mockstubs
		ctx.SetupStubs()
		fmt.Println(ctx.UserStub.GetChannelID())
	} else {
		// Definition of the Fabric SDK properties
		setupJSON, err := ioutil.ReadFile(utils.ConfigLocation)
		if err != nil {
			fmt.Println(err)
			for true {
				fmt.Println(err)
			}
			return
		}
		err = json.Unmarshal(setupJSON, &ctx.ClientSetup)
		ctx.ClientSetup.ConfigFile = utils.HyperledgerConfigLocation
		if err != nil {
			fmt.Println(err)
			return
		}
		// Initialization of the Fabric SDK from the previously set properties
		err = ctx.ClientSetup.Initialize()
		if err != nil {
			fmt.Printf("Unable to initialize the Fabric SDK: %v\n", err)
			fmt.Println(ctx.ClientSetup.OrgAdmin)
			return
		}
		defer ctx.ClientSetup.CloseSDK()
	}

	//Handle endpoints
	router := gin.Default()
	router.GET("/user/:ID", rhf.GetUserHandlerFactory(&ctx))
	router.POST("/user/identity", rhf.UpdateIdentityFileHandlerFactory(&ctx))
	router.POST("/user", rhf.RegisterHandlerFactory(&ctx))

	router.GET("/block/:ipfshash", rhf.GetBlockHandlerFactory(&ctx))
	router.POST("/block", rhf.AddBlockHandlerFactory(&ctx))
	router.POST("/blocks", rhf.ListHandlerFactory(&ctx))
	router.POST("/block/permissions", rhf.AddPermissionHandlerFactory(&ctx))

	router.GET("/key/:ID", rhf.GetKeyHandlerFactory(&ctx))
	router.POST("/key", rhf.StoreKeyHandlerFactory(&ctx))

	router.POST("/assets/newAsset", rhf.NewAssetHandlerFactory(&ctx))
	router.POST("/assets/addMinters", rhf.AddMintersHandlerFactory(&ctx))
	router.POST("/assets/removeMinters", rhf.RemoveMintersHandlerFactory(&ctx))
	router.POST("/assets/getMinters", rhf.GetMintersHandlerFactory(&ctx))
	router.POST("/assets/changeAdmin", rhf.ChangeAdminHandlerFactory(&ctx))
	router.POST("/assets/mint", rhf.MintHandlerFactory(&ctx))
	router.POST("/assets/burn", rhf.BurnHandlerFactory(&ctx))
	router.POST("/assets/totalSupply", rhf.TotalSupplyHandlerFactory(&ctx))
	router.POST("/assets/balanceOf", rhf.BalanceOfHandlerFactory(&ctx))
	router.POST("/assets/balanceOfAdmin", rhf.BalanceOfAdminHandlerFactory(&ctx))
	router.POST("/assets/transfer", rhf.TransferHandlerFactory(&ctx))
	router.POST("/assets/transferToAdmin", rhf.TransferToAdminHandlerFactory(&ctx))
	router.POST("/assets/transferFromAdmin", rhf.TransferFromAdminHandlerFactory(&ctx))
	router.POST("/nuke", nukeHandlerFactory(&ctx))
	router.POST("/", badGatewayHandler)

	fmt.Println("Starting...")
	router.Run(":8080")

}
