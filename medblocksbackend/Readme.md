# Medblocks REST API server

# Overview

    Exposes a REST API for medblocks operation on the hyperledger network. It connects to hyperledger as a client using the provided credentials and configurations, and calls chaincode on the client's behalf.

# API operations

    This section details the API operations provided and the inputs and outputs expected.

## Data:signature model

    Input to endpoints which follow this model is expected in the following format
    {
        "data": "json representation of input data",
        "signature": "signature of the data string"
    }

## User and Data management operations

### POST /user

    Registers a user on the chain. Uses Data:Signature model

    Input:
    {
        "emailId":"id",
        "sPublicKey":"PEM-encoded PCKS1v15 public key used for signatures",
        "ePublicKey":"PEM-encoded PCKS1v15 public key used for encryption",
        "metadata":{
            optional field, metadata object
        }
    }

    Output:
    returns the json representation of the user object as stored on the blockchain
    {
        "emailId":"id",
        "sPublicKey":"PEM-encoded PCKS1v15 public key used for signatures",
        "ePublicKey":"PEM-encoded PCKS1v15 public key used for encryption",
        "metadata":{
            optional field, metadata object
        }
    }

### GET /user/{emailId}

    Gets the user object from the chain. Can be used to fetch public keys.
    Input: 
    empty
    
    Output:
    returns the json representation of the user object as stored on the blockchain
    {
        "emailId":"id",
        "sPublicKey":"PEM-encoded PCKS1v15 public key used for signatures",
        "ePublicKey":"PEM-encoded PCKS1v15 public key used for encryption",
        "metadata":{
            optional field, metadata object
        }
    }

### POST /user/identity

    Updates the identityFileHash for the user on the chain. Uses Data:Signature model

    Input:
    {
        "emailId":"id",
        "identityFileHash":"IPFSHash of identity file to be set"
    }

    Output:
    returns the json representation of the user object as stored on the blockchain
    {
        "emailId":"id",
        "identityFileHash":"IPFSHash of identity file to be set"
    }

### POST /key

    TEMPORARY FUNCTION
    Stores user's private keys on the backend database. Private keys are expected to be encrypted with a user's password before storage.

    Input:
    {
        "emailId":"id",
        "sPrivateKey":"Encrypted PCKS1v15 private key used for signatures",
        "ePrivateKey":"Encrypted PCKS1v15 private key used for encryption"
    }

    Output:
    returns the json representation of the user object as stored on the database
    {
        "emailId":"id",
        "sPrivateKey":"Encrypted PCKS1v15 private key used for signatures",
        "ePrivateKey":"Encrypted PCKS1v15 private key used for encryption"
    }

### GET /key

    TEMPORARY FUNCTION
    Gets user's private keys from the backend database. Private keys are expected to be encrypted with a user's password before storage.

    Input:
    empty

    Output:
    returns the json representation of the user object as stored on the database
    {
        "emailId":"id",
        "sPrivateKey":"Encrypted PCKS1v15 private key used for signatures",
        "ePrivateKey":"Encrypted PCKS1v15 private key used for encryption"
    }

### POST /block

    Adds blocks to the chain. Uses Data:Signature model.
    Block needs to be symmetrically encrypted with AES before being put on IPFS. IPFS hash that is generated needs to be passed along with multi-party encrypted AES keys.

    Input:
    [
        {
            "ipfsHash":"ipfsHash of the block",
            "name":"name of block",
            "format":"data format of block",
            "ownerEmailId":"emailID of owner of block",
            "signatoryEmailId":"emailID of signatory",
            "IV":"IV used while encrypting the block with AES",
            "permissions":[
                {
                    "receiverEmailId":"emailId of a permitted user",
                    "receiverKey":"AESKey encrypted with public Key of receiver"
                },
                {
                    ...
                }
            ]
        },
        {
            ...
        }
    ]

    Output:
    [   
        {
            "ipfsHash": "ipfsHash of block",
            "message": "Contains block object if it was successfully inserted",
            "error": "contains error statement if block was not successfully inserted"
        },
        {
            ...
        }
    ]

### POST /block/permissions

    Adds permissions to a block on the chain. Uses Data:Signature model.

    Input:
    {
        "ipfsHash":"ipfsHash of the block",
        "senderEmailId":"emailId sending this request",
        "permissions":[
            {
                "receiverEmailId":"emailId of a permitted user",
                "receiverKey":"AESKey encrypted with public Key of receiver"
            },
            {
                ...
            }
        ]
    }

    Output:
    [
        {
            "receiverEmailId":"emailId of a permitted user",
            "receiverKey":"AESKey encrypted with public Key of receiver"
        },
        {
            ...
        }
    ]

### GET /block/{ipfsHash}

    Gets block object by ipfsHash

    Input: nil
    Output: 
    {
        "ipfsHash":"ipfsHash of the block",
        "name":"name of block",
        "format":"data format of block",
        "ownerEmailId":"emailID of owner of block",
        "signatoryEmailId":"emailID of signatory",
        "IV":"IV used while encrypting the block with AES",
        "permissions":[
            {
                "receiverEmailId":"emailId of a permitted user",
                "receiverKey":"AESKey encrypted with public Key of receiver"
            },
            {
                ...
            }
        ]
    }

### POST /blocks

    Gets block object by filtering parameters, creatorEmailId and permittedEmailId

    Input: 
    {
        "permittedEmailId": "emailId with access to a block",
        "ownerEmailId": "emailId of owner of block"
    }

    both fields are optional. if neither are supplied, empty {} must be passed and list of all blocks is returned. passing either or both will filter results accordingly.
    Output: 
    [
        {
            "ipfsHash":"ipfsHash of the block",
            "name":"name of block",
            "format":"data format of block",
            "ownerEmailId":"emailID of owner of block",
            "permissions":[
                {
                    "receiverEmailId":"emailId of a permitted user",
                    "receiverKey":"AESKey encrypted with public Key of receiver"
                },
                {
                    ...
                }
            ]
        }
    ]

## Asset management operations

# Code Documentation

## cmd

Contains files built into executables for the project

### MedblocksBackend.go

Contains exec code for the API

## config

Contains configuration files which will be parsed by the code

## internal

Contains packages specific to API code and which should not be imported outside of this project (compiler respects "internal" folder name).

### database

Contains Code for database operations. Currently only to store and receive private keys.

### requesthandlerfunctions

Contains Code that generates Handler functions for the various API endpoints, also some local supporting functions.

### structs

Contains structs that are used across the project and some management functions for the structs.

### test

Contains test functions that are called in test mode. These functions run requests on MockShim for testing chaincode.

## pkg

Contains general packages written which may be used in other projects

### hyperledgerinterface

General interface for interfacing with a hyperledgerfabric node

### utils

General Utilities used.

## vendor

Contains vendor packages
