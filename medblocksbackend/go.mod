module gitlab.com/medblocks/gomedblocks/medblocksbackend

go 1.12

require (
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/cloudflare/cfssl v0.0.0-20190510060611-9c027c93ba9e // indirect
	github.com/fjl/go-couchdb v0.0.0-20140704151333-1f327c218d24
	github.com/gin-gonic/gin v1.4.0
	github.com/golang/mock v1.3.1 // indirect
	github.com/google/certificate-transparency-go v1.0.21 // indirect
	github.com/hyperledger/fabric v1.4.3
	github.com/hyperledger/fabric-lib-go v1.0.0 // indirect
	github.com/hyperledger/fabric-sdk-go v1.0.0-alpha5
	github.com/pkg/errors v0.8.1
	gitlab.com/medblocks/hyperledgerchaincode v0.0.0-20191026181601-24c9e9665467

)
